#version 430



uniform sampler2D iChannel0;
uniform sampler2D iChannel1;
uniform sampler2D iChannel2;
uniform sampler2D iChannel3;
uniform sampler2D iChannelBack;
uniform vec2 iResolution;
uniform float iGlobalTime;
uniform vec4 iMouse;
uniform vec4 iDate;
uniform float iTick;

in vec4 in_Position;

out vec4 pass_fragColor;
out vec2 pass_fragCoord;

void main(void) {
	gl_Position = vec4(in_Position.x, -in_Position.y, in_Position.z, in_Position.w);
	float ratio = iResolution.x / iResolution.y;
	vec2 u = in_Position.xy;
	u.y = -u.y;
	vec2 pass_vertexCoord = (u + 1.) / 2.;
	pass_fragCoord = iResolution * pass_vertexCoord;
	pass_fragColor = vec4(0);
}