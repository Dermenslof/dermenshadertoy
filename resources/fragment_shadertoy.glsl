#version 430

#extension GL_NV_shadow_samplers_cube : enable

uniform sampler2D iChannel0;
uniform sampler2D iChannel1;
uniform sampler2D iChannel2;
uniform sampler2D iChannel3;
uniform sampler2D iChannelBack;
uniform vec2 iResolution;
uniform float iGlobalTime;
uniform vec4 iMouse;
uniform vec4 iDate;
uniform float iTick;
 
in vec4 pass_fragColor;
in vec2 pass_fragCoord;

out vec4 out_color;

void mainImage(out vec4 fragColor ,in vec2 fragCoord)
{
	vec2 uv = fragCoord.xy / iResolution.xy;
	fragColor = vec4(uv,0.5+0.5*sin(iGlobalTime),1.0);
}

void main(void) {
	out_color = pass_fragColor;
  	mainImage(out_color, pass_fragCoord);
}