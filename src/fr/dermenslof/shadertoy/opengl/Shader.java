package fr.dermenslof.shadertoy.opengl;

import static org.lwjgl.opengl.GL20.glDeleteShader;
import static org.lwjgl.opengl.GL20.glDetachShader;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;

public class Shader
{
	private String fragmentSource;
	public String error;
	public int vsId;
	public int fsId;

	public Shader(String fragmentSource)
	{
		this.fragmentSource = fragmentSource;
	}
	
	public Shader create()
	{
		String shaderSource = getShaderSource("resources/vertex_shadertoy.glsl");
		vsId = this.loadShader(shaderSource, GL20.GL_VERTEX_SHADER);
		if (fragmentSource == null)
			shaderSource = getShaderSource("resources/fragment_shadertoy.glsl");
		else
			shaderSource = this.fragmentSource;
		fsId = this.loadShader(shaderSource, GL20.GL_FRAGMENT_SHADER);
		return this;
	}
	
	private int loadShader(String shaderSource, int type)
	{
		int shaderID = GL20.glCreateShader(type);
		GL20.glShaderSource(shaderID, shaderSource);
		GL20.glCompileShader(shaderID);
		if (GL20.glGetShaderi(shaderID, GL20.GL_COMPILE_STATUS) == GL11.GL_FALSE)
		{
			error = GL20.glGetShaderInfoLog(shaderID, 99999);
			System.err.println(error);
			System.err.println("Could not compile shader.");
			return 0;
		}
		return shaderID;
	}

	private String getShaderSource(String filename)
	{
		filename = System.getProperty("user.dir") + "/" + filename;
		StringBuilder shaderSource = new StringBuilder();
		try
		{
			BufferedReader reader = new BufferedReader(new FileReader(filename));
			String line;
			while ((line = reader.readLine()) != null)
				shaderSource.append(line).append("\n");
			reader.close();
		}
		catch (IOException e)
		{
			error = "Could not read file: " + filename;
			System.err.println("Could not read file.");
			e.printStackTrace();
		}
		return shaderSource.toString();
	}
	
	public boolean isValid()
	{
		return error != null;
	}
	
	public void delete(int pId)
	{
		glDetachShader(pId, vsId);
		glDetachShader(pId, fsId);
		glDeleteShader(fsId);
		glDeleteShader(vsId);
	}
}