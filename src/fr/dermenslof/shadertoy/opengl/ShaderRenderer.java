package fr.dermenslof.shadertoy.opengl;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL12.*;
import static org.lwjgl.opengl.GL13.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.opengl.GLCanvas;
import org.eclipse.swt.opengl.GLData;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL;

import com.github.sarxos.webcam.Webcam;

import de.matthiasmann.twl.utils.PNGDecoder;
import de.matthiasmann.twl.utils.PNGDecoder.Format;
import fr.dermenslof.shadertoy.resource.BasicResource;
import fr.dermenslof.shadertoy.resource.ResourceManager;
import fr.dermenslof.shadertoy.resource.data.ChannelInput.Type;
import fr.dermenslof.shadertoy.sound.util.SoundPlayer;
import fr.dermenslof.shadertoy.ui.element.ShaderEditor;

public class ShaderRenderer extends GLCanvas
{
	private static GLData		glData;

	private int					iResolution, iGlobalTime, iMouse, iDate, iTick, iChannelBack;
	private String				source;
	private int[]				iChannels		= new int[] { 0, 0, 0, 0 };
	public int					pId				= 0;
	private int[]				texIds			= new int[] { 0, 0, 0, 0 };
	private SoundPlayer[]		players			= new SoundPlayer[4];
	private ByteBuffer[]		playerBuffers	= new ByteBuffer[4];
	private Thread[]			threadsPlayer	= new Thread[4];
	public Type[]				texTypes		= new Type[] { Type.TEXTURE, Type.TEXTURE, Type.TEXTURE, Type.TEXTURE };
	private long				startTime;

	private int					lastWidth, lastHeight;
	private boolean				hasError		= false;
	public String				error			= null;
	public boolean				pause			= false;
	public long					pauseDelay, startPause;
	private long				lastFPS;
	private int					fps;
	public int					tick			= 1;
	private int					reTick			= 1;
	private long				lastTick;
	private float				startTick;
	private int					fpsCounter;
	public float				lastGlobalTime;
	private float				lastYear, lastMonth, lastDay, lastHTime;
	private boolean				MouseDown;
	private int					lastMouseX, lastMouseY, currentMouseX, currentMouseY;
	private int					backId;
	private ByteBuffer			backBuffer;
	public boolean				lastBackBuffer	= false;
	private DecimalFormat		df;
	private Shader				shader;
	public static boolean		isReady;
	private ShaderEditor		editor;
	public Webcam				webcam;
	private Capture				capture;
	private int					filter, wrap;
	private boolean				fl;

	static
	{
		glData = new GLData();
		glData.doubleBuffer = true;
	}

	public ShaderRenderer(ShaderEditor editor)
	{
		super(editor.shell, SWT.NONE | SWT.NO_BACKGROUND, glData);
		this.editor = editor;
	}

	public void makeCurrent()
	{
		setCurrent();
	}

	private void doResize()
	{
		Rectangle rec = getClientArea();
		int w = Math.max(rec.width, 1);
		int h = Math.max(rec.height, 1);
		editor.textWidth.setText("" + w);
		editor.textHeight.setText("" + (h + 5));
		makeCurrent();
		glViewport(0, 0, w, h);
		lastWidth = w;
		lastHeight = h;
		backBuffer = null;
	}

	public void onstart()
	{
		startTime = getTime();
		this.lastFPS = getTime();
	}

	public void onFrame()
	{
		updateLogic();
		if (!hasError)
			this.render();
		updateFPS();
	}

	public void init()
	{
		makeCurrent();
		GL.createCapabilities();

		addListener(SWT.Resize, new Listener()
		{
			public void handleEvent(Event event)
			{
				doResize();
			}
		});
		
		addMouseListener(new MouseListener()
		{
			@Override
			public void mouseUp(MouseEvent event)
			{
				if (event.button == 1)
				{
					MouseDown = false;
					lastMouseX = 0;
					lastMouseY = 0;
					currentMouseX = event.x;
					currentMouseY = lastHeight - event.y;
				}
			}

			@Override
			public void mouseDown(MouseEvent event)
			{
				if (event.button == 1)
				{
					MouseDown = true;
					lastMouseX = event.x;
					lastMouseY = lastHeight - event.y;
					currentMouseX = event.x;
					currentMouseY = lastHeight - event.y;
				}
			}
			@Override
			public void mouseDoubleClick(MouseEvent arg0){}
		});
		addMouseMoveListener(new MouseMoveListener()
		{
			@Override
			public void mouseMove(MouseEvent event)
			{
				if (MouseDown)
				{
					currentMouseX = event.x;
					currentMouseY = lastHeight - event.y;
				}
			}
		});

		DecimalFormatSymbols sym = new DecimalFormatSymbols();
		sym.setDecimalSeparator('.');
		sym.setGroupingSeparator(',');
		df = new DecimalFormat("0.00", sym);
		glClearColor(1.0f, 1.0f, 1.0f, 0.0f); // using alpha = 0.0 is important

		createQuad();
		reloadShaders(null);
	}

	private void createQuad()
	{
		int vboiId = glGenBuffers();
		int vaoId = glGenBuffers();
		vaoId = glGenBuffers();
		float[] vertices = { 
				-1, -1, 0,
				1, -1, 0,
				1,  1, 0,
				-1,  1, 0
		};
		byte[] indices = { 0, 1, 2, 2, 3, 0 };
		glBindBuffer(GL_ARRAY_BUFFER, vboiId);
		glBufferData(GL_ARRAY_BUFFER, (FloatBuffer) BufferUtils.createFloatBuffer(vertices.length).put(vertices).flip(), GL_STATIC_DRAW);
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vaoId);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, (ByteBuffer) BufferUtils.createByteBuffer(indices.length).put(indices).flip(), GL_STATIC_DRAW);
		glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0L);
	}

	private boolean validateProgram(int program, Shader shader, boolean delete)
	{
		String programLog = glGetProgramInfoLog(program);
		if (programLog.trim().length() > 0)
			System.err.println("WARNING: " + programLog);
		int linked = glGetProgrami(program, GL_LINK_STATUS);
		if (linked == 0)
		{
			int errorValue = glGetError();
			if (errorValue != GL_NO_ERROR)
			{
				String errorType = "UNKNOW_ERROR";
				switch (errorValue)
				{
					case GL_INVALID_ENUM:
						errorType = "GL_INVALID_ENUM";
						break;
					case GL_INVALID_VALUE:
						errorType = "GL_INVALID_VALUE";
						break;
					case GL_INVALID_OPERATION:
						errorType = "GL_INVALID_OPERATION";
						break;
					case GL_INVALID_FRAMEBUFFER_OPERATION:
						errorType = "GL_INVALID_FRAMEBUFFER_OPERATION";
						break;
					case GL_OUT_OF_MEMORY:
						errorType = "GL_OUT_OF_MEMORY";
						break;
				}
				shader.delete(program);
				error = "Could not link program: " + errorType;
				hasError = true;
				return false;
			}
		}
		if (delete)
		{
			shader.delete(program);
			if (pId > 0)
				glDeleteProgram(pId);
		}
		pId = program;
		return true;
	}

	private String formatChannels(String sourceShader)
	{
		if (sourceShader == null)
			return null;
		for (int i = 0; i < 4; ++i)
			sourceShader = sourceShader.replaceAll("<ch" + i + ">", texTypes[i] != Type.CUBEMAP ? "sampler2D" : "samplerCube");
		return sourceShader;
	}
	
	public void reloadShaders(String sourceShader)
	{
		glUseProgram(0);
		isReady = false;
		hasError = false;
		error = null;
		shader = new Shader(formatChannels(sourceShader)).create();
		if (shader.error != null)
		{
			hasError = true;
			error = shader.error;
		}
		int program = glCreateProgram();
		glAttachShader(program, shader.vsId);
		glAttachShader(program, shader.fsId);
		glBindAttribLocation(program, 0, "in_Position");
		glLinkProgram(program);
		if (validateProgram(program, shader, sourceShader == null))
		{
			source = sourceShader;
			glValidateProgram(pId);
			glUseProgram(pId);
			iResolution = glGetUniformLocation(pId, "iResolution");
			iGlobalTime = glGetUniformLocation(pId, "iGlobalTime");
			iMouse = glGetUniformLocation(pId, "iMouse");
			iDate = glGetUniformLocation(pId, "iDate");
			iTick = glGetUniformLocation(pId, "iTick");
			for (int i = 0; i < 4; ++i)
				iChannels[i] = glGetUniformLocation(pId, "iChannel" + i);
			iChannelBack = glGetUniformLocation(pId, "iChannelBack");
			isReady = true;
			glUseProgram(0);
		}
	}

	private int loadBackBuffer()
	{
		backId = glGenTextures();
		glActiveTexture(GL_TEXTURE0 + 4);
		glBindTexture(GL_TEXTURE_2D, backId);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, lastWidth, lastHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, backBuffer);
		glGenerateMipmap(GL_TEXTURE_2D);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		backBuffer.flip();
		backBuffer.clear();
		return backId;
	}

	public void loadPNGTexture(InputStream in, int id, int filter, int wrap, boolean flip)
	{
		if (in == null || texIds[id] != 0)
		{
			glDeleteTextures(texIds[id]);
			texIds[id] = 0;
			if (in == null)
				return;
		}
		ByteBuffer buf = null;
		int tWidth = 0;
		int tHeight = 0;
		PNGDecoder decoder = null;
		try
		{
			decoder = new PNGDecoder(in);
			tWidth = decoder.getWidth();
			tHeight = decoder.getHeight();
			buf = ByteBuffer.allocateDirect(4 * tWidth * tHeight);
			if (flip)
				decoder.decodeFlipped(buf, decoder.getWidth() * 4, Format.RGBA);
			else
				decoder.decode(buf, decoder.getWidth() * 4, Format.RGBA);
			buf.flip();
			in.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return;
		}
		int texId = glGenTextures();
		texIds[id] = texId;
		texTypes[id] = Type.TEXTURE;
		glActiveTexture(GL_TEXTURE0 + id);
		glBindTexture(GL_TEXTURE_2D, texId);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, tWidth, tHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, buf);
		glGenerateMipmap(GL_TEXTURE_2D);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filter);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filter);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrap);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrap);
		reloadShaders(source);
	}
	
	public void loadPNGTexture(InputStream in, int id)
	{
		loadPNGTexture(in, id, GL_LINEAR, GL_REPEAT, true);
	}
	
	public void loadCubeMap(String name, int id, int filter, int wrap, boolean flip)
	{
		if (texIds[id] != 0)
		{
			glDeleteTextures(texIds[id]);
			texIds[id] = 0;
		}
		String[] tab = name.split("/");
		if (tab.length > 1)
			name = tab[tab.length - 1];
		else
			name = tab[0];
		name = name.split("_")[0];
		InputStream in = null;
		ByteBuffer[] buf = new ByteBuffer[6];
		int tWidth = 0;
		int tHeight = 0;
		PNGDecoder[] decoder = new PNGDecoder[6];
		try
		{
			for (int i = 0; i < 6; ++i)
			{
				in = ResourceManager.getAsInputStream(new BasicResource("resources/cubemaps/" + name + "_" + i + ".png"));
				decoder[i] = new PNGDecoder(in);
				tWidth = decoder[i].getWidth();
				tHeight = decoder[i].getHeight();
				buf[i] = ByteBuffer.allocateDirect(4 * tWidth * tHeight);
				if (flip)
					decoder[i].decodeFlipped(buf[i], decoder[i].getWidth() * 4, Format.RGBA);
				else
					decoder[i].decode(buf[i], decoder[i].getWidth() * 4, Format.RGBA);
				buf[i].flip();
				in.close();
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return;
		}
		int texId = glGenTextures();
		texIds[id] = texId;
		texTypes[id] = Type.CUBEMAP;
//		glActiveTexture(GL_TEXTURE0 + id);
		glBindTexture(GL_TEXTURE_CUBE_MAP, id);
		 
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, filter);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, wrap);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, wrap);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, wrap);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, GL_RGBA, decoder[0].getWidth(), decoder[0].getHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, buf[0]);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, GL_RGBA, decoder[1].getWidth(), decoder[1].getHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, buf[1]);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, GL_RGBA, decoder[2].getWidth(), decoder[2].getHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, buf[2]);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, GL_RGBA, decoder[3].getWidth(), decoder[3].getHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, buf[3]);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, GL_RGBA, decoder[4].getWidth(), decoder[4].getHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, buf[4]);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, GL_RGBA, decoder[5].getWidth(), decoder[5].getHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, buf[5]);
		
		glGenerateMipmap(GL_TEXTURE_CUBE_MAP);
		reloadShaders(source);
	}
	
	public void loadCubeMap(String name, int id)
	{
		loadCubeMap(name, id, GL_LINEAR, GL_CLAMP_TO_EDGE, true);
	}
	
	public synchronized int loadCamTexture(int id)
	{
		if (texIds[id] != 0)
		{
			glDeleteTextures(texIds[id]);
			texIds[id] = 0;
		}
		ByteBuffer buf = fl ? capture.flipped : capture.normal;
		int tWidth = 0;
		int tHeight = 0;
		try
		{
			tWidth = Webcam.getDefault().getViewSize().width;
			tHeight = Webcam.getDefault().getViewSize().height;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return 0;
		}
		if (buf == null)
			return 0;
		int texId = glGenTextures();
		texIds[id] = texId;
		glActiveTexture(GL_TEXTURE0 + id);
		glBindTexture(GL_TEXTURE_2D, texId);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, tWidth, tHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, buf);
		glGenerateMipmap(GL_TEXTURE_2D);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filter);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filter);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrap);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrap);
		buf.flip();
		return texId;
	}
	
	public int loadCamTexture(int id, int filter, int wrap, boolean fl)
	{
		this.filter = filter;
		this.wrap = wrap;
		this.fl = fl;
		return loadCamTexture(id);
	}
	
	public void enableWebCam(int id)
	{
		try
		{
			capture = new Capture();
			capture.start();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return;
		}
		filter = GL_LINEAR;
		wrap = GL_REPEAT;
		fl = true;
		if (texTypes[id] == Type.CUBEMAP)
			reloadShaders(source);
		texTypes[id] = Type.WEBCAM;
	}
	
	public void disableWebCam(int id)
	{
		if (capture != null)
			capture.cancel();
		if (texIds[id] != 0)
			glDeleteTextures(texIds[id]);
		texIds[id] = 0;
		texTypes[id] = Type.TEXTURE;
			
	}
	
	public synchronized int loadSoundTexture(int id, ByteBuffer buf)
	{
		if (pause)
			return texIds[id];
		if (texIds[id] != 0)
		{
			glDeleteTextures(texIds[id]);
			texIds[id] = 0;
		}
		
//		int tWidth = players[id].fft.specSize() - 1;
//		float[] tab = players[id].player.mix.toArray();
//		int len = tab.length;
//		ByteBuffer buf = ByteBuffer.allocateDirect(3 * len);
//		if (buf == null)
//			return 0;
//		players[id].fft.forward(players[id].player.mix);
//		for(int i = 0; i < len; i++)
//		{
//			float b = players[id].fft.getBand((tWidth - (i / 2)));
//			buf.put((byte)(b * 255f));
//			buf.put((byte)((tab[i] < 0 ? 0 : tab[i]) * 255f));
//			buf.put((byte)(players[id].player.mix.level() * 255f));
//		}
//		buf.flip();
		
		if (buf == null)
			return 0;
		int texId = glGenTextures();
		texIds[id] = texId;
		glActiveTexture(GL_TEXTURE0 + id);
		glBindTexture(GL_TEXTURE_2D, texId);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, players[id].player.mix.size(), 1, 0, GL_RGBA, GL_UNSIGNED_BYTE, buf);
		glGenerateMipmap(GL_TEXTURE_2D);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		buf.clear();
		return texId;
	}
	
	public void setSound(final int id, final SoundPlayer player)
	{
		texTypes[id] = player == null ? Type.TEXTURE : Type.SOUND;
		if (player == null && texIds[id] != 0)
		{
			glDeleteTextures(texIds[id]);
			texIds[id] = 0;
		}
		players[id] = player;
		if (threadsPlayer[id] != null && threadsPlayer[id].isAlive())
			threadsPlayer[id].interrupt();
		if (player != null)
		{
			threadsPlayer[id] = new Thread("SoundTexture iChannel" + 0)
			{
				@Override
				public void run()
				{
					while (players[id] != null)
					{
						try
						{
							int tWidth = players[id].fft.specSize();
							float[] tab = players[id].player.mix.toArray();
							int len = tab.length;
							ByteBuffer buf = ByteBuffer.allocateDirect(4 * len);
							players[id].fft.forward(players[id].player.mix);
							for(int i = 0; i < len; i++)
							{
								float b = players[id].fft.getBand(tWidth - i * tWidth / len);
								buf.put((byte)(b * 255f));
								buf.put((byte)((tab[i] / 2 + .5) * 255f));
								buf.put((byte)(players[id].player.left.level() * 255f));
								buf.put((byte)(players[id].player.right.level() * 255f));
							}
							buf.flip();
							playerBuffers[id] = buf.duplicate();
							buf.clear();
						}
						catch (Exception e)
						{
						}
					}
				}
			};
			threadsPlayer[id].start();
		}
	}

	private void bindTextures()
	{
		for (int i = 0; i < 4; ++i)
		{
			switch (texTypes[i])
			{
				case TEXTURE:
					glUniform1i(iChannels[i], i);
					glActiveTexture(GL_TEXTURE0 + i);
					glBindTexture(GL_TEXTURE_2D, texIds[i]);
					break;
				case SOUND:
					glUniform1i(iChannels[i], i);
					int id = loadSoundTexture(i, playerBuffers[i]);
					glActiveTexture(GL_TEXTURE0 + i);
					glBindTexture(GL_TEXTURE_2D, id);
					break;
				default:
					break;
			}
		}
		for (int i = 0; i < 4; ++i)
		{
			if (texTypes[i] != Type.WEBCAM)
				continue;
			glUniform1i(iChannels[i], i);
			int id = loadCamTexture(i);
			glActiveTexture(GL_TEXTURE0 + i);
			glBindTexture(GL_TEXTURE_2D, id);
			break;
		}
		glUniform1i(iChannelBack, 4);
		glActiveTexture(GL_TEXTURE0 + 4);
		glBindTexture(GL_TEXTURE_2D, backId);
		for (int i = 0; i < 4; ++i)
		{
			if (texTypes[i] != Type.CUBEMAP)
				continue;
			glUniform1i(iChannels[i], i);
			glBindTexture(GL_TEXTURE_CUBE_MAP, texIds[i]);
		}
	}

	private void bindUniformLocations()
	{
		glUniform2f(iResolution, lastWidth, lastHeight);
		glUniform1f(iGlobalTime, lastGlobalTime);
		glUniform4f(iDate, lastYear, lastMonth, lastDay, lastHTime);
		glUniform4f(iMouse, currentMouseX, currentMouseY, lastMouseX, lastMouseY);
		glUniform1f(iTick, startTick);
	}

	public void resetGlobalTime()
	{
		startTime = getTime();
		lastGlobalTime = 0;
		pauseDelay = 0;
		startPause = 0;
		lastTick = getTime();
	}

	public void setGlobalTime(long time)
	{
		startTime = getTime() - time * 1000L;
		lastGlobalTime = time / 1000000f;
		pauseDelay = 0;
		startPause = 0;
		forcePause();
	}

	public void pause()
	{
		pause = !pause;
		if (pause)
			forcePause();
		else if (lastGlobalTime > 0)
			pauseDelay += getTime() - startPause;
		else
			startTime = getTime();
	}

	public void forcePause()
	{
		pause = true;
		startPause= getTime();
	}

	private long getTime()
	{
		return System.nanoTime();
	}

	private void updateFPS()
	{
		if (hasError)
			editor.timeLabel.setText("ERROR");
		else
		{
			String time = df.format(lastGlobalTime);
			if (time.endsWith("."))
				time += "" + 0;
			editor.timeLabel.setText("FPS: " + fpsCounter + " -- Time: " + time);
		}
		if (getTime() - lastFPS > 1000000000L)
		{
			fpsCounter = fps;
			fps = 0;
			lastFPS += 1000000000L;
		}
		fps++;
	}

	private void updateHTime()
	{
		if (pause)
			return;
		lastYear = getYear();
		lastMonth = getMonth();
		lastDay = getDay();
		lastHTime = getHTime();
	}

	private float getYear()
	{
		return (float)Calendar.getInstance(TimeZone.getTimeZone(Locale.FRANCE.getDisplayName())).get(Calendar.YEAR);
	}

	private float getMonth()
	{
		return (float)Calendar.getInstance(TimeZone.getTimeZone(Locale.FRANCE.getDisplayName())).get(Calendar.MONTH);
	}

	private float getDay()
	{
		return (float)Calendar.getInstance(TimeZone.getTimeZone(Locale.FRANCE.getDisplayName())).get(Calendar.DAY_OF_MONTH);
	}

	private float getHTime()
	{
		return (Calendar.getInstance(TimeZone.getTimeZone(Locale.FRANCE.getDisplayName())).getTimeInMillis() % (1000L * 60L * 60L * 24L)) / 1000.0f + (60.0f * 60.0f);
	}

	private void updateLogic()
	{
		if (!pause)
		{
			updateHTime();
			long time = getTime() - startTime - pauseDelay;
			lastGlobalTime = time / 1000000000.0f;
		}
		startTick = 1f;
		if (!pause)
		{
			startTick = 0f;
			if (tick == 1 || reTick-- > 0)
			{
				startTick = 1f;
				lastTick = getTime();
			}
			else if (!pause && getTime() - lastTick > tick * 1000000L)
			{
				reTick = 1;
				startTick = 1f;
				lastTick = getTime();
			}
		}
	}

	private void render()
	{
		if (!isReady || pId < 1)
			return;
		glUseProgram(pId);
		
		bindUniformLocations();
		bindTextures();

		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_BYTE, 0);
		
		//backBuffer
		if (!pause && lastBackBuffer && !ShaderEditor.activeBackBuffer)
		{
			if (backBuffer == null)
				backBuffer = BufferUtils.createByteBuffer(lastWidth * lastHeight * 4);
			lastBackBuffer = false;
			glDeleteTextures(backId);
			loadBackBuffer();
		}
		if (!pause && ShaderEditor.activeBackBuffer)
		{
			if (backBuffer == null)
				backBuffer = BufferUtils.createByteBuffer(lastWidth * lastHeight * 4);
			lastBackBuffer = true;
			glDeleteTextures(backId);
			glReadBuffer(GL_FRONT);
			glReadPixels(0, 0, lastWidth, lastHeight, GL_RGBA, GL_UNSIGNED_BYTE, backBuffer);
			loadBackBuffer();
		}
		//end backBuffer

		glUseProgram(0);
	}
}

class Capture extends Thread
{
	public ByteBuffer normal;
	public ByteBuffer flipped;
	private Webcam webcam;
	private boolean alive;
	
	public Capture() throws Exception
	{
		super("capture");
		this.webcam = Webcam.getDefault();
		if (!webcam.isOpen())
		{
			webcam.setViewSize(webcam.getViewSizes()[webcam.getViewSizes().length - 1]);
			webcam.open();
		}
		if (!webcam.isOpen())
			throw new Exception();
		alive = true;
	}

	@Override
	public void run()
	{
		while (alive)
		{
			if (!webcam.isOpen())
				break;
			flip();
		}
		if (webcam != null && webcam.isOpen())
			webcam.close();
	}
	
	public void cancel()
	{
		alive = false;
	}
	
	public void flip()
	{
		BufferedImage bi = webcam.getImage();
		byte[] normalPixels = null;
		byte[] flippedPixels = null;
        if (bi != null)
        {
        	int w = bi.getWidth();
        	int h = bi.getHeight();
        	byte[] img = ((DataBufferByte)bi.getRaster().getDataBuffer()).getData();
        	normalPixels = new byte[(w *= 3) * h];
        	flippedPixels = new byte[w * h];
            for (int y = 0; y < h; y++)
            {
            	for (int x = 0; x < w; x += 3)
            	{
            		normalPixels[((h - y - 1) * w) + x] = img[(y * w) + x];
            		normalPixels[((h - y - 1) * w) + x + 1] = img[(y * w) + x + 1];
            		normalPixels[((h - y - 1) * w) + x + 2] = img[(y * w) + x + 2];
            		flippedPixels[((h - y - 1) * w) + w - (x + 2) - 1] = img[(y * w) + x];
            		flippedPixels[((h - y - 1) * w) + w - (x + 1) - 1] = img[(y * w) + x + 1];
            		flippedPixels[((h - y - 1) * w) + w - x - 1] = img[(y * w) + x + 2];
                }
            };
            normal = ByteBuffer.allocateDirect(w * 3 * h);
            normal.put(normalPixels);
            normal.flip();
            flipped = ByteBuffer.allocateDirect(w * 3 * h);
            flipped.put(flippedPixels);
            flipped.flip();
        }
	}
}