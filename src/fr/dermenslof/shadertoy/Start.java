package fr.dermenslof.shadertoy;

import org.eclipse.wb.swt.SWTResourceManager;

import fr.dermenslof.shadertoy.resource.data.ChannelInput;
import fr.dermenslof.shadertoy.resource.data.IconInput;
import fr.dermenslof.shadertoy.sound.util.SoundCloud;
import fr.dermenslof.shadertoy.ui.element.ShaderEditor;

public class Start
{
	public static void main(String[] args)
	{
		System.setProperty("org.lwjgl.librarypath", System.getProperty("user.dir") + "/libs/native");
		new Thread(new Runnable() {
			@Override
			public void run()
			{
				ShaderEditor.iplay = SWTResourceManager.getImage(IconInput.PLAY.thumbnail);
				ShaderEditor.ipause = SWTResourceManager.getImage(IconInput.PAUSE.thumbnail);
				ShaderEditor.irewind = SWTResourceManager.getImage(IconInput.REWIND.thumbnail);
				ChannelInput.init();
				SoundCloud.init();
			}
		}).start();
		ShaderEditor se = new ShaderEditor();
		se.onCreate();
		se.run();
	}
}