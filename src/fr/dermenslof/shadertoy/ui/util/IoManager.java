package fr.dermenslof.shadertoy.ui.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class IoManager
{
	@SuppressWarnings("resource")
	public static String getFile(String filename) throws IOException
	{
		InputStream in = new BufferedInputStream(new FileInputStream(filename));
		StringBuffer buf = new StringBuffer();
		int c;
		while ((c = in.read()) != -1)
			buf.append((char) c);
		return buf.toString();
	}

	public static void saveFile(String filename, byte[] data) throws IOException
	{
		File outputFile = new File(filename);
		FileOutputStream out = new FileOutputStream(outputFile);
		out.write(data);
		out.close();
	}
}