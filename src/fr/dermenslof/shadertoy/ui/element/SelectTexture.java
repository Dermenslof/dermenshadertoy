package fr.dermenslof.shadertoy.ui.element;

import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.events.ShellListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import fr.dermenslof.shadertoy.resource.data.ChannelInput;
import fr.dermenslof.shadertoy.resource.data.ChannelInput.Type;
import fr.dermenslof.shadertoy.resource.data.IconInput;
import fr.dermenslof.shadertoy.sound.util.SoundCloud;
import fr.dermenslof.shadertoy.sound.util.SoundCloudThread;

public class SelectTexture extends Dialog
{
	public Object				result;
	public Shell				shell;
	protected SoundCloudThread	sdThread;
	private Listener			keylistener;

	public SelectTexture(Shell parent, int style)
	{
		super(parent, style);
		setText("Textures");
	}

	public Object open()
	{
		createContents();
		shell.open();
		shell.layout();
		Display display = getParent().getDisplay();
		while (!shell.isDisposed())
		{
			if (!display.readAndDispatch())
				display.sleep();
		}
		display.removeFilter(SWT.KeyDown, keylistener);
		return result;
	}

	private void fillButtons(Composite composite, ChannelInput[] inputs, RowData rowData)
	{
		for (final ChannelInput input : inputs)
		{
			Button button = new Button(composite, SWT.NONE);
			if (input.type != Type.WEBCAM)
			{
				Image image = input.small_thumbnail;
				button.setImage(image);
			}
			else
			{
				button.setToolTipText("WebCam");
				button.setImage(SWTResourceManager.getImageResize(IconInput.WEBCAM.thumbnail, 64, 64));
			}
			button.setLayoutData(new RowData(64, 64));
			button.addSelectionListener(new SelectionAdapter()
			{
				@Override
				public void widgetSelected(SelectionEvent event)
				{
					result = input;
					shell.close();
				}
			});
		}
	}

	public void createContents()
	{
		shell = new Shell(getParent(), SWT.DIALOG_TRIM | SWT.MAX | SWT.APPLICATION_MODAL);
		shell.setSize(800, 600);
		shell.setText(getText());
		shell.setBackground(new Color(Display.getCurrent(), 44, 44, 44));
		shell.setForeground(new Color(Display.getCurrent(), 200, 200, 200));
		shell.setLayout(new FillLayout(SWT.VERTICAL));

		shell.addShellListener(new ShellListener()
		{
			public void shellIconified(ShellEvent arg0) {}
			public void shellDeiconified(ShellEvent arg0) {}
			public void shellDeactivated(ShellEvent arg0) {}
			public void shellActivated(ShellEvent arg0) {}

			@Override
			public void shellClosed(ShellEvent event)
			{
				if (sdThread != null && sdThread.isAlive())
					event.doit = false;
			}
		});

		{
			Composite misc = new Composite(shell, SWT.NONE);
			misc.setBackground(new Color(Display.getCurrent(), 44, 44, 44));
			misc.setForeground(new Color(Display.getCurrent(), 200, 200, 200));
			misc.setLayout(new RowLayout(SWT.HORIZONTAL));
			Label label = new Label(misc, SWT.NONE);
			label.setText("Misc");
			label.setBackground(new Color(Display.getCurrent(), 44, 44, 44));
			label.setForeground(new Color(Display.getCurrent(), 200, 200, 200));
			Button button = new Button(misc, SWT.NONE);
			button.setBackground(new Color(Display.getCurrent(), 44, 44, 44));
			button.setForeground(new Color(Display.getCurrent(), 200, 200, 200));
			button.setText("none");
			button.setLayoutData(new RowData(64, 64));
			button.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent event)
				{
					result = "none";
					shell.close();
				}
			});
			fillButtons(misc, ChannelInput.WEBCAM, new RowData(64, 64));

			Group group = new Group(misc, SWT.SHADOW_IN | SWT.DIALOG_TRIM);
			group.setText("SoundCloud");
			group.setLayoutData(new RowData(290, 45)); // 40
			GridLayout layout = new GridLayout();
			layout.marginLeft = layout.marginTop = layout.marginRight = layout.marginBottom = 0;
			layout.verticalSpacing = 0;
			layout.horizontalSpacing = 10;
			layout.numColumns = 2;
			group.setLayout(layout);
			final Text soundUri = new Text(group, SWT.SINGLE);
			button = new Button(group, SWT.PUSH);
			final ProgressBar bar = new ProgressBar(group, SWT.BORDER);
			keylistener = new Listener()
			{
	            public void handleEvent(Event event)
	            {
					if (event.keyCode == 'v' && ((event.stateMask & SWT.CTRL) == SWT.CTRL))
					{
						if (soundUri.isFocusControl())
							return;
						Transferable transferable = Toolkit.getDefaultToolkit().getSystemClipboard().getContents(null);
						try
						{
							if (transferable != null && transferable.isDataFlavorSupported(DataFlavor.stringFlavor))
							{
								String text = (String) transferable.getTransferData(DataFlavor.stringFlavor);
								soundUri.setText(text);
							}
						}
						catch (Exception e) {}
					}
	            }
	        };
			shell.getDisplay().addFilter(SWT.KeyDown, keylistener);
			soundUri.setText("https://");
			soundUri.selectAll();
			soundUri.addKeyListener(new KeyAdapter()
			{
				@Override
				public void keyPressed(KeyEvent event)
				{
					if (event.keyCode == SWT.CR)
					{
						String uri = soundUri.getText().trim();
						if (!uri.isEmpty() && !uri.equals("https://") && (sdThread == null || !sdThread.isAlive()))
						{
							sdThread = new SoundCloudThread(bar.getDisplay(), new SoundCloud(uri), bar, SelectTexture.this);
							sdThread.start();
						}
					}
				}
			});
			button.setText("Ok");
			bar.setMinimum(0);
			bar.setMaximum(100);
			bar.setVisible(false);
			button.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent event)
				{
					String uri = soundUri.getText().trim();
					if (!uri.isEmpty() && !uri.equals("https://") && (sdThread == null || !sdThread.isAlive()))
					{
						sdThread = new SoundCloudThread(bar.getDisplay(), new SoundCloud(uri), bar, SelectTexture.this);
						sdThread.start();
					}
				}
			});
			GridData gdata = new GridData();
			gdata.widthHint = 200;
			soundUri.setLayoutData(gdata);
			gdata = new GridData();
			gdata.widthHint = 64;
			button.setLayoutData(gdata);
			gdata = new GridData();
			gdata.widthHint = 200;
			bar.setLayoutData(gdata);
			soundUri.pack();
			button.pack();
			bar.pack();
			group.pack();
		}

		{
			Composite textures = new Composite(shell, SWT.DIALOG_TRIM | SWT.MIN);
			textures.setBackground(new Color(Display.getCurrent(), 44, 44, 44));
			textures.setForeground(new Color(Display.getCurrent(), 200, 200, 200));
			textures.setLayout(new RowLayout(SWT.HORIZONTAL));
			Label label = new Label(textures, SWT.NONE);
			label.setBackground(new Color(Display.getCurrent(), 44, 44, 44));
			label.setForeground(new Color(Display.getCurrent(), 200, 200, 200));
			label.setText("Textures");
			textures.setLayout(new RowLayout(SWT.HORIZONTAL));
			fillButtons(textures, ChannelInput.TEXTURES, new RowData(64, 64));
		}

		{
			Composite cubemaps = new Composite(shell, SWT.DIALOG_TRIM | SWT.MIN);
			cubemaps.setBackground(new Color(Display.getCurrent(), 44, 44, 44));
			cubemaps.setForeground(new Color(Display.getCurrent(), 200, 200, 200));
			cubemaps.setLayout(new RowLayout(SWT.HORIZONTAL));
			Label label = new Label(cubemaps, SWT.NONE);
			label.setText("Cubemaps");
			label.setBackground(new Color(Display.getCurrent(), 44, 44, 44));
			label.setForeground(new Color(Display.getCurrent(), 200, 200, 200));
			fillButtons(cubemaps, ChannelInput.CUBEMAPS, new RowData(64, 64));
		}
	}
}
