package fr.dermenslof.shadertoy.ui.element;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;
import static java.nio.file.StandardWatchEventKinds.OVERFLOW;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.LogManager;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ExpandEvent;
import org.eclipse.swt.events.ExpandListener;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.ExpandBar;
import org.eclipse.swt.widgets.ExpandItem;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Scale;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import com.github.sarxos.webcam.Webcam;

import fr.dermenslof.shadertoy.opengl.ShaderRenderer;
import fr.dermenslof.shadertoy.resource.ResourceManager;
import fr.dermenslof.shadertoy.resource.data.ChannelInput;
import fr.dermenslof.shadertoy.ui.util.IoManager;

public class ShaderEditor
{
	public Shell					shell;
	protected Display				display;
	public String					filename;
	public String					shaderCode;
	protected MiniPlayer[]			miniPlayers		= new MiniPlayer[4];
	protected ChannelInput[]		channels		= new ChannelInput[4];
	protected Combo[]				filters			= new Combo[4];
	protected Combo[]				wraps			= new Combo[4];
	protected Button[]				boxes			= new Button[4];
	protected ChannelInputButton[]	buttons			= new ChannelInputButton[4];
	protected ExpandBar[]			options			= new ExpandBar[4];
	protected ExpandItem[]			items			= new ExpandItem[4];
	public Composite				tools;

	private static ShaderEditor		app;
	public ShaderRenderer			renderer;
	public static Image				iplay, ipause, irewind;
	private Button					playButton, backBufferButton;
	private Scale					timeSlider;
	private TickSlider				tickSlider;
	private static String			error			= null;
	public static boolean			activeBackBuffer;
	public Label					timeLabel;

	private WatchService			watcher;
	private WatchKey				key;
	private Path					dir;

	public Text						textWidth;
	public Text						textHeight;

	public Color					BACKGROUND_SHELL;
	public Color					BACKGROUND_DEFAULT;

	private static String			DEFAULT_SHADER	= "" + "\n"
			+ "void\t\tmainImage(out vec4 fragColor, in vec2 fragCoord)\n"
			+ "{\n"
			+ "\tvec2 uv = fragCoord.xy / iResolution.xy;\n"
			+ "\tfragColor = vec4(uv,0.5+0.5*sin(iGlobalTime),1.0);\n"
			+ "}";
	
	private static String			pattern 		= ""
			+ "#version 430\n\n"
			+ "\n"
			+ "#extension GL_NV_shadow_samplers_cube : enable\n"
			+ "\n"
			+ "uniform <ch0> iChannel0;\n"
			+ "uniform <ch1> iChannel1;\n"
			+ "uniform <ch2> iChannel2;\n"
			+ "uniform <ch3> iChannel3;\n"
			+ "uniform sampler2D iChannelBack;\n"
			+ "uniform vec2 iResolution;\n"
			+ "uniform float iGlobalTime;\n"
			+ "uniform vec4 iMouse;\n"
			+ "uniform vec4 iDate;\n"
			+ "uniform float iTick = 0;\n"
			+ "\n"
			+ "in vec4 pass_fragColor;\n"
			+ "in vec2 pass_fragCoord;\n"
			+ "out vec4 out_color;\n"
			+ "\n"
			+ "<pattern>"
			+ "\n\n"
			+ "void main(void)\n{\n"
			+ "\tout_color = vec4(0);\n"
			+ "\tmainImage(out_color, pass_fragCoord);\n"
			+ "}\n";
 
	public static ShaderEditor getApp()
	{
		return app;
	}

	public ShaderEditor()
	{
		LogManager.getLogManager().reset();
		app = this;
		display = new Display();
	}

	private void initColors()
	{
		BACKGROUND_SHELL = shell.getBackground();
		RGB rgb = BACKGROUND_SHELL.getRGB();
		if (rgb.red == 0 && rgb.red == 0 && rgb.green == 0)
			BACKGROUND_SHELL = new Color(Display.getCurrent(), 50, 50, 50);
		rgb = BACKGROUND_SHELL.getRGB();
		if (rgb.red > 125 && rgb.green > 125 && rgb.blue > 125)
			BACKGROUND_DEFAULT = new Color(Display.getCurrent(), new RGB(rgb.red - 50, rgb.green - 50,rgb.blue - 50));
		else
			BACKGROUND_DEFAULT = new Color(Display.getCurrent(), new RGB(rgb.red - 10, rgb.green - 10,rgb.blue - 10));
	}
	
	public void onCreate()
	{
		shell = new Shell(display, SWT.DIALOG_TRIM | SWT.MAX | SWT.RESIZE /*| SWT.NO_BACKGROUND*/ | SWT.NO_REDRAW_RESIZE);
		initColors();
		shell.setBackground(BACKGROUND_SHELL);
		shell.setLocation(0, 0);
		shell.setLayout(new FormLayout());
		shell.setMenuBar(new EditorMenu(shell).getMenu());
		shell.setMinimumSize(806, 270);
		renderer = new ShaderRenderer(this);
		renderer.init();
		createContents(shell);
	}

	public void run()
	{
		shell.open();
		shell.setSize(802, 801);
		renderer.onstart();
		start();
		while (!shell.isDisposed())
		{
			handleFile();
			if (!display.readAndDispatch())
				display.sleep();
		}
		exit();
	}

	private void createContents(final Shell shell)
	{
		Label label = null, ref = null;
		FormData data = null;
		
		tools = new Composite(shell, SWT.NONE);
		tools.setBackground(BACKGROUND_SHELL);
		tools.setLayout(new FormLayout());
		data = new FormData();
		data.left = new FormAttachment(0);
		data.right = new FormAttachment(100);
		data.bottom = new FormAttachment(100);
		tools.setLayoutData(data);
		for (int i = 0; i < 4; ++i)
		{
			options[i] = new ExpandBar(tools, SWT.V_SCROLL);
			options[i].setBackground(BACKGROUND_DEFAULT);
			data = new FormData();
			data.left = new FormAttachment(1, i * 200);
			data.bottom = new FormAttachment(100);
			data.height = 25;// 112
			data.width = 182;
			options[i].setLayoutData(data);

			Composite composite = new Composite(options[i], SWT.NONE);
			composite.setBackground(BACKGROUND_DEFAULT);
			GridLayout layout = new GridLayout();
			layout.marginLeft = layout.marginTop = layout.marginRight = layout.marginBottom = 0;
			layout.verticalSpacing = 0;
			layout.horizontalSpacing = 0;
			layout.numColumns = 2;
			composite.setLayout(layout);
			data = new FormData();
			data.width = 150;
			composite.setLayoutData(data);

			Label title = new Label(composite, SWT.NONE);
			title.setBackground(BACKGROUND_DEFAULT);
			title.setText("filter:");
			filters[i] = new Combo(composite, SWT.NONE);
			filters[i].add("linear", 0);
			filters[i].add("nearest", 1);
			filters[i].add("mipmap", 2);
			filters[i].select(0);
			GridData gdata = new GridData();
			gdata.widthHint = 120;
			filters[i].setLayoutData(gdata);

			title = new Label(composite, SWT.NONE);
			title.setBackground(BACKGROUND_DEFAULT);
			title.setText("wrap:");
			wraps[i] = new Combo(composite, SWT.MIN);
			wraps[i].add("repeat", 0);
			wraps[i].add("clamp", 1);
			wraps[i].select(0);
			gdata = new GridData();
			gdata.widthHint = 120;
			wraps[i].setLayoutData(gdata);

			boxes[i] = new Button(composite, SWT.CHECK);
			boxes[i].setBackground(BACKGROUND_DEFAULT);
			boxes[i].setText("flip");
			boxes[i].setSelection(true);
			KeyAdapter kl = new KeyAdapter()
			{
				@Override
				public void keyPressed(KeyEvent event)
				{
					event.doit = event.keyCode == SWT.ARROW_DOWN || event.keyCode == SWT.ARROW_UP;
				}
			};
			filters[i].addKeyListener(kl);
			wraps[i].addKeyListener(kl);

			final int j = i;
			SelectionListener l = new SelectionListener()
			{
				@Override
				public void widgetSelected(SelectionEvent event)
				{
					if (channels[j] == null)
						return;
					int f = GL11.GL_LINEAR;
					int w = GL11.GL_REPEAT;
					boolean fl = boxes[j].getSelection();
					switch (filters[j].getSelectionIndex())
					{
						case 0:
							f = GL11.GL_LINEAR;
							break;
						case 1:
							f = GL11.GL_NEAREST;
							break;
					}
					switch (wraps[j].getSelectionIndex())
					{
						case 0:
							w = GL11.GL_REPEAT;
							break;
						case 1:
							w = GL12.GL_CLAMP_TO_EDGE;
							break;
					}
					if (channels[j].type == ChannelInput.Type.TEXTURE)
						renderer.loadPNGTexture(ResourceManager.getAsInputStream(channels[j].resource), j, f, w, fl);
					else if (channels[j].type == ChannelInput.Type.CUBEMAP)
						renderer.loadCubeMap(channels[j].resource.getPath(), j, f, w, fl);
					else if (channels[j].type == ChannelInput.Type.WEBCAM)
						renderer.loadCamTexture(j, f, w, fl);
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent event)
				{
				}
			};
			filters[i].addSelectionListener(l);
			wraps[i].addSelectionListener(l);
			boxes[i].addSelectionListener(l);

			items[i] = new ExpandItem(options[i], SWT.NONE, 0);
			items[i].setText("options");
			items[i].setHeight(composite.computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
			items[i].setControl(composite);

			data = new FormData();
			data.left = new FormAttachment(1, i * 200);
			data.bottom = new FormAttachment(options[i]);
			data.height = 112;
			data.width = 180;
			buttons[i] = new ChannelInputButton(tools, SWT.BORDER, i);
			buttons[i].setLayoutData(data);
			data.left = new FormAttachment(1, i * 200 + 1);
			buttons[i].label.setLayoutData(data);
			buttons[i].initLabel();
			
			miniPlayers[i] = new MiniPlayer(tools, SWT.NONE);
			data = new FormData();
			data = new FormData();
			data.left = new FormAttachment(1, i * 200);
			data.bottom = new FormAttachment(options[i]);
			data.height = 30;
			data.width = 30;
			miniPlayers[i].play.setLayoutData(data);

			data = new FormData();
			data = new FormData();
			data.left = new FormAttachment(miniPlayers[i].play);
			data.bottom = new FormAttachment(options[i]);
			data.height = 30;
			data.width = 30;
			miniPlayers[i].rewind.setLayoutData(data);

			data = new FormData();
			data = new FormData();
			data.left = new FormAttachment(miniPlayers[i].rewind);
			data.bottom = new FormAttachment(options[i]);
			data.height = 30;
			data.width = 120;
			miniPlayers[i].volume.setLayoutData(data);

			data = new FormData();
			data.left = new FormAttachment(1, i * 200);
			data.bottom = new FormAttachment(miniPlayers[i].play);
			data.height = 52; // 112
			data.width = 180;
			miniPlayers[i].setLayoutData(data);

			data = new FormData();
			data.left = new FormAttachment(1, i * 200);
			data.bottom = new FormAttachment(miniPlayers[i]);
			data.height = 30;
			data.width = 180;
			miniPlayers[i].change.setLayoutData(data);
			miniPlayers[i].change.setText("Change");
			miniPlayers[i].change.addSelectionListener(new TextureSelectionAdapter(i, this));

			miniPlayers[i].setIsVisible(false);

			data = new FormData();
			data.left = new FormAttachment(1, i * 200);
			data.bottom = new FormAttachment(buttons[i]);
			data.height = 25;
			data.width = 182;
			ref = new Label(tools, SWT.NONE);
			ref.setBackground(BACKGROUND_DEFAULT);
			ref.setText("\t      iChannel" + i);
			ref.setLayoutData(data);
			options[i].addExpandListener(new ExpandListener()
			{
				@Override
				public void itemExpanded(ExpandEvent event)
				{
					if (miniPlayers[j].isReady())
						miniPlayers[j].setIsVisible(false);
					else
						buttons[j].setVisible(false);
					ExpandBar b = (ExpandBar) event.widget;
					Rectangle rec = buttons[j].getBounds();
					b.setLocation(rec.x, rec.y);
					b.setBounds(rec.x, rec.y, 182, 142);
				}

				@Override
				public void itemCollapsed(ExpandEvent event)
				{
					if (miniPlayers[j].isReady())
						miniPlayers[j].setIsVisible(true);
					else
					{
						if (buttons[j].label.getImage() != null)
							buttons[j].setVisible(false, true);
						else
							buttons[j].setVisible(true, false);
					}
					ExpandBar b = (ExpandBar) event.widget;
					Rectangle rec = buttons[j].getBounds();
					b.setBounds(rec.x, rec.y + rec.height, 182, 30);
				}
			});
		}

		shell.addListener(SWT.Resize, new Listener()
		{
			public void handleEvent(Event event)
			{
				for (int i = 0; i < 4; ++i)
				{
					if (!buttons[i].isVisible() && !buttons[i].label.isVisible())
					{
						if (miniPlayers[i].isReady())
							miniPlayers[i].setIsVisible(true);
						else
						{
							if (buttons[i].label.getImage() != null)
								buttons[i].setVisible(false, true);
							else
								buttons[i].setVisible(true, false);
						}
						items[i].setExpanded(false);
					}
				}
			}
		});

		timeSlider = new Scale(tools, SWT.BORDER | SWT.NONE);
		timeSlider.setBackground(BACKGROUND_DEFAULT);
		timeSlider.setMinimum(0);
		timeSlider.setMaximum(60000010);
		data = new FormData();
		data.left = new FormAttachment(0, 2);
		data.right = new FormAttachment(100, -2);
		data.bottom = new FormAttachment(ref, -55);
		timeSlider.setLayoutData(data);
		timeSlider.addSelectionListener(new SelectionListener()
		{
			@Override
			public void widgetSelected(SelectionEvent event)
			{
				if (!renderer.pause)
					renderer.pause();
				updateButtonPlay();
				if (renderer.pause)
					renderer.setGlobalTime(timeSlider.getSelection());
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent event)
			{
			}
		});

		timeLabel = new Label(tools, SWT.NONE);
		timeLabel.setBackground(BACKGROUND_SHELL);
		timeLabel.setText("FPS: 0 -- Time: 0.0");
		data = new FormData();
		data.left = new FormAttachment(0);
		data.right = new FormAttachment(100);
		data.bottom = new FormAttachment(timeSlider);
		timeLabel.setLayoutData(data);

		data = new FormData();
		data.left = new FormAttachment(0);
		data.right = new FormAttachment(100);
		data.top = new FormAttachment(shell.getMenuBar().getParent());
		data.bottom = new FormAttachment(tools);
		data.height = 20;
		renderer.setLayoutData(data);

		Button button = new Button(tools, SWT.NONE);
		button.setText("Run");
		button.setToolTipText("Alt+Enter");
		data = new FormData();
		data.left = new FormAttachment(0);
		data.bottom = new FormAttachment(ref, -12);
		data.height = 28;
		data.width = 50;
		button.setLayoutData(data);
		button.addListener(SWT.Selection, new Listener()
		{
			@Override
			public void handleEvent(Event e)
			{
				switch (e.type)
				{
					case SWT.Selection:
						reloadShader();
						break;
				}
			}
		});

		Button rewind = new Button(tools, SWT.NONE);
		rewind.setImage(irewind);
		data = new FormData();
		data.left = new FormAttachment(button);
		data.bottom = new FormAttachment(ref, -12);
		data.height = 28;
		data.width = 30;
		rewind.setLayoutData(data);
		rewind.addListener(SWT.Selection, new Listener()
		{
			@Override
			public void handleEvent(Event e)
			{
				switch (e.type)
				{
					case SWT.Selection:
						renderer.resetGlobalTime();
						timeSlider.setSelection(0);
						break;
				}
			}
		});

		playButton = new Button(tools, SWT.NONE);
		playButton.setImage(ipause);
		playButton.setSelection(true);
		data = new FormData();
		data.left = new FormAttachment(rewind);
		data.bottom = new FormAttachment(ref, -12);
		data.height = 28;
		data.width = 30;
		playButton.setLayoutData(data);
		playButton.addListener(SWT.Selection, new Listener()
		{
			@Override
			public void handleEvent(Event e)
			{
				switch (e.type)
				{
					case SWT.Selection:
						renderer.pause();
						updateButtonPlay();
						break;
				}
			}
		});

		backBufferButton = new Button(tools, SWT.CHECK);
		backBufferButton.setBackground(BACKGROUND_SHELL);
		backBufferButton.setTextDirection(SWT.LEFT_TO_RIGHT);
		backBufferButton.setText("backBuffer");
		data = new FormData();
		data.left = new FormAttachment(playButton);
		data.bottom = new FormAttachment(ref, -12);
		backBufferButton.setLayoutData(data);
		backBufferButton.addListener(SWT.Selection, new Listener()
		{
			@Override
			public void handleEvent(Event e)
			{
				activeBackBuffer = !activeBackBuffer;
			}
		});

		textWidth = new Text(tools, SWT.BORDER);
		textHeight = new Text(tools, SWT.BORDER);
		KeyAdapter kl = new KeyAdapter()
		{
			@Override
			public void keyPressed(KeyEvent event)
			{
				if (event.keyCode == SWT.CR)
				{
					if (!textWidth.getText().isEmpty() && !textHeight.getText().isEmpty())
						shell.setSize(Integer.valueOf(textWidth.getText()) + 2, Integer.valueOf(textHeight.getText()) + 321);
				}
				else if (!Character.isDigit(event.character))
				{
					if (event.character >= 32 && event.character < 127)
						event.doit = false;
				}
			}
		};
		textHeight.addKeyListener(kl);
		textWidth.addKeyListener(kl);
		textWidth.setTextLimit(4);
		textHeight.setTextLimit(4);
		Button validate = new Button(tools, SWT.NONE);
		validate.setText("Send");;
		data = new FormData();
		data.right = new FormAttachment(100);
		data.bottom = new FormAttachment(ref, -12);
		data.height = 28;
		validate.setLayoutData(data);
		validate.addListener(SWT.Selection, new Listener()
		{
			@Override
			public void handleEvent(Event e)
			{
				if (!textWidth.getText().isEmpty() && !textHeight.getText().isEmpty())
					shell.setSize(Integer.valueOf(textWidth.getText()) + 2, Integer.valueOf(textHeight.getText()) + 321);

			}
		});

		data = new FormData();
		data.right = new FormAttachment(validate);
		data.bottom = new FormAttachment(ref, -12);
		data.height = 16;
		data.width = 60;
		textHeight.setLayoutData(data);
		label = new Label(tools, SWT.NONE);
		label.setBackground(BACKGROUND_SHELL);
		label.setText("height:");
		data = new FormData();
		data.right = new FormAttachment(textHeight);
		data.bottom = new FormAttachment(ref, -12);
		data.height = 26;
		label.setLayoutData(data);
		data = new FormData();
		data.right = new FormAttachment(label, -4);
		data.bottom = new FormAttachment(ref, -12);
		data.height = 16;
		data.width = 60;
		textWidth.setLayoutData(data);
		label = new Label(tools, SWT.NONE);
		label.setBackground(BACKGROUND_SHELL);
		label.setText("width:");
		data = new FormData();
		data.right = new FormAttachment(textWidth);
		data.bottom = new FormAttachment(ref, -12);
		data.height = 26;
		label.setLayoutData(data);

		tickSlider = new TickSlider(this);
		tickSlider.setVisible(true);
		data = new FormData();
		data.left = new FormAttachment(backBufferButton, 5);
		data.right = new FormAttachment(label, -5);
		data.bottom = new FormAttachment(ref, -12);
		tickSlider.setLayoutData(data);
		data = new FormData();
		data.left = new FormAttachment(backBufferButton, 5);
		data.bottom = new FormAttachment(tickSlider);
		tickSlider.button.setLayoutData(data);
		tickSlider.button.setBackground(BACKGROUND_SHELL);
		data = new FormData();
		data.left = new FormAttachment(tickSlider.button, 5);
		data.bottom = new FormAttachment(tickSlider);
		data.height = 20;
		tickSlider.label.setLayoutData(data);
		tickSlider.label.setBackground(BACKGROUND_SHELL);

		shaderCode = DEFAULT_SHADER;
	}

	public void start()
	{
		display.syncExec(new Runnable()
		{
			public void run()
			{
				for (int i = 0; i < 4; ++i)
				{
					MiniPlayer mp = miniPlayers[i];
					if (mp == null)
						continue;
					mp.updateButton();
				}
				if (error != null)
				{
					updateButtonPlay();
					showError(error);
					error = null;
				}
				if (!renderer.isDisposed())
				{
					if (!renderer.pause && renderer.lastGlobalTime < 60)
						timeSlider.setSelection((int) (renderer.lastGlobalTime * 1000000f));
					renderer.makeCurrent();
					renderer.onFrame();
					renderer.swapBuffers();
					display.asyncExec(this);
				}
			}
		});
	}

	private void handleFile()
	{
		if (filename == null || key == null)
			return;
		for (WatchEvent<?> event : key.pollEvents())
		{
			WatchEvent.Kind<?> kind = event.kind();

			if (kind == OVERFLOW)
				continue;
			@SuppressWarnings("unchecked")
			WatchEvent<Path> ev = (WatchEvent<Path>) event;
			Path filename = ev.context();
			if (filename.toString().equals(shell.getText()))
			{
				reloadShader();
				break;
			}
		}
	}

	public void reloadShader()
	{
		try
		{
			if (filename != null)
				shaderCode = IoManager.getFile(filename).replaceAll("//(.{1})", "// $1");
		}
		catch (IOException e1)
		{
			e1.printStackTrace();
		}
		renderer.reloadShaders(pattern.replace("<pattern>", shaderCode));
		Timer t = new Timer();
		t.schedule(new TimerTask()
		{
			@Override
			public void run()
			{
				if (renderer.error != null)
				{
					renderer.forcePause();
					String msg = renderer.error;
					String[] split = msg.split("\n");
					StringBuilder sb = new StringBuilder();
					for (int i = 0; i < split.length; ++i)
					{
						String l = split[i];
						Pattern pat = Pattern.compile("^0.(.[0-9]*.)");
						Matcher matcher = pat.matcher(l);
						while (matcher.find())
						{
							String tmp = l.substring(matcher.start() + 2, matcher.end() - 1);
							l = l.replace(matcher.group(), "line=>" + (Integer.valueOf(tmp) - 20));
							break;
						}
						sb.append(l + "\n");
					}
					error = sb.toString();
				}
				this.cancel();
			}
		}, 200L);
	}

	public void updateButtonPlay()
	{
		if (renderer.pause)
			playButton.setImage(iplay);
		else
			playButton.setImage(ipause);
	}

	public void openFile()
	{
		FileDialog dlg = new FileDialog(shell);
		dlg.setFilterExtensions(new String[]
			{ "*.glsl", "*.GLSL" });
		String temp = dlg.open();
		if (temp != null)
		{
			filename = temp;
			String[] tab = filename.split("/");
			shell.setText(tab.length > 0 ? tab[tab.length - 1] : "untitled");
			activeBackBuffer = false;
			backBufferButton.setSelection(false);
			renderer.resetGlobalTime();
			reloadShader();
			key = null;
			try
			{
				watcher = FileSystems.getDefault().newWatchService();
				dir = new File(filename).getParentFile().toPath();
				key = dir.register(watcher, ENTRY_CREATE, ENTRY_MODIFY);
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}

	public void showError(String error)
	{
		showError(shell, error);
	}

	public void showError(Shell shell, String error)
	{
		MessageBox mb = new MessageBox(shell, SWT.ICON_ERROR | SWT.OK);
		mb.setMessage(error);
		mb.open();
	}

	public void showInfo(String info)
	{
		MessageBox mb = new MessageBox(shell, SWT.ICON_INFORMATION | SWT.OK);
		mb.setMessage(info);
		mb.open();
	}
	
	public void exit()
	{
		for (int i = 0; i < 4; ++i)
		{
			MiniPlayer mp = miniPlayers[i];
			if (mp != null)
			{
				renderer.setSound(i, null);
				if (mp.sPlayer != null)
					mp.sPlayer.close();
			}
		}
		try
		{
			Webcam wc = Webcam.getDefault();
			if (wc.isOpen())
				wc.close();
//			display.dispose();
		}
		catch (Exception ex)
		{
			// silent error
		}
		System.exit(0);
	}
}