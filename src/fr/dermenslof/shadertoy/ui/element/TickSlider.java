package fr.dermenslof.shadertoy.ui.element;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Scale;

public class TickSlider extends Scale
{
	private ShaderEditor editor;
	public Button button;
	public Label label;
	public boolean mode;
	
	public TickSlider(final ShaderEditor editor)
	{
		super(editor.tools, SWT.HORIZONTAL);
		this.editor = editor;
		this.button = new Button(editor.tools, SWT.CHECK);
		this.label = new Label(editor.tools, SWT.NONE);
		
		label.setFont(new Font(Display.getCurrent(), editor.shell.getFont().getFontData()[0].getName(), 8, SWT.NONE));
		
		setMinimum(10);
		setMaximum(1000);
		setSelection(0);
		updateLabel();
		
		button.addListener(SWT.Selection, new Listener()
		{
			@Override
			public void handleEvent(Event e)
			{
				toggleMode();
			}
		});
		
		addSelectionListener(new SelectionListener()
		{
			@Override
			public void widgetSelected(SelectionEvent event)
			{
				updateLabel();
				editor.renderer.tick = getMs();
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent event) {}
		});
		
		addMouseListener(new MouseListener()
		{
			boolean pause;
			@Override
			public void mouseUp(MouseEvent arg0)
			{
				if (!pause)
				{
					editor.renderer.pause();
					editor.updateButtonPlay();
				}
			}
			
			@Override
			public void mouseDown(MouseEvent arg0)
			{
				pause = editor.renderer.pause;
				editor.renderer.forcePause();
				editor.updateButtonPlay();
			}
			
			@Override
			public void mouseDoubleClick(MouseEvent arg0)
			{
				;	
			}
		});

	}

	public void setVisible(boolean visible)
	{
		super.setVisible(visible);
		label.setVisible(visible);
		button.setVisible(visible);
	}
	
	public void updateLabel()
	{
		if (!mode)
			label.setText("Tick duration : " + getValue() + " ms");
		else
		{
			int value = 100 - getSelection();
			if (value < 1)
				value = 1;
			label.setText("Tick" + (value == 1 ? "" : "s") + " per second : " + (((int)curve(1f, 100f, (float)value))));
		}
	}
	
	public void toggleMode()
	{
		mode = !mode;
		int value = getSelection();
		setMinimum(!mode ? 10 : 0);
		setMaximum(!mode ? 1000 : 100);
		setSelection(!mode ? value * 10 : (value == 10 ? 0 : value / 10));
		updateLabel();
		redraw();
		editor.renderer.tick = getMs();
	}
	
	public int getValue(boolean shift)
	{
		int value = getSelection();
		if (value < 1)
			value = 1;
		if (!mode)
		{
			value = (value / 10) * 10;
			if (value < 10)
				value = 10;
		}
		return value;
	}

	private int getMs()
	{
		int ms = getValue();
		if (!mode)
			return ms;
		ms = 100 - getSelection();
		return 1000 / (ms < 1 ? 1 : ms);
	}
	
	public int getValue()
	{
		return getValue(false);
	}
	
	private float clamp(Float f, float min, float max)
	{
		if (f < min)
			return min;
		if (f > max)
			return max;
		return f;
	}
	
	private float curve(float edge0, float edge1, float x)
	{
	    Float f = clamp((x - edge0)/(edge1 - edge0), 0.0f, 1.0f); 
	    f = (float) (Math.sqrt(f) * f * f * f * f);
	    return f.isInfinite() || f.isNaN() ? 1f : x;
	}
	
	@Override
	protected void checkSubclass() {}
}
