package fr.dermenslof.shadertoy.ui.element;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;

public class InputsBox extends Dialog
{
	private static String INPUTS = ""
		    + "uniform vec3\t\tiResolution;\t\t// viewport resolution (in pixels)\n"
		    + "uniform float\t\tiGlobalTime;\t\t// shader playback time (in seconds)\n"
		    + "uniform vec4\t\tiMouse;\t\t\t// mouse pixel coords. xy: current (if MLB down), zw: click\n"
		    + "uniform vec4\t\tiDate;\t\t\t// (year, month, day, time in seconds)\n"
		    + "uniform sampler2D\tiChannel0;\n"
		    + "uniform sampler2D\tiChannel1;\n"
		    + "uniform sampler2D\tiChannel2;\n"
		    + "uniform sampler2D\tiChannel3;\n"
		    + "uniform sampler2D\tiChannelBack;\t\t// the last frame\n"
		    + "uniform float\t\tiTick;\t\t\t// 1f for the first frame of a tick. otherwise 0f\n";
	
	public void centerShellOnScreen(final Shell shell)
	{
		final Rectangle shellBounds = shell.getBounds();
		final Display display = shell.getDisplay();
		final Monitor primaryMonitor = display.getPrimaryMonitor();
		final Rectangle monitorRect = primaryMonitor.getClientArea();
		final int x = monitorRect.x + (monitorRect.width  - shellBounds.width)  / 2;
		final int y = monitorRect.y + (monitorRect.height - shellBounds.height) / 2;
		shell.setLocation(x, y);
	}

	public InputsBox()
	{
		super(Display.getDefault().getActiveShell());
	}
	
	@Override
	protected void createButtonsForButtonBar(final Composite parent)
	{ 
	  GridLayout layout = (GridLayout)parent.getLayout();
	  layout.marginHeight = 0;
	}

	@Override
	protected Control createDialogArea(final Composite parent)
	{
		final Composite dialogArea = (Composite) super.createDialogArea(parent);
		final Label label = new Label(dialogArea, SWT.NULL);
		dialogArea.setBackground(new Color(Display.getCurrent(), 59, 59, 59));
		dialogArea.setForeground(new Color(Display.getCurrent(), 200, 200, 200));
		label.setBackground(new Color(Display.getCurrent(), 59, 59, 59));
		label.setForeground(new Color(Display.getCurrent(), 200, 200, 200));
		label.setText(INPUTS);
		label.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true));
		return dialogArea;
	}

	@Override
	protected void configureShell(final Shell shell)
	{
		super.configureShell(shell);
		shell.setBackground(new Color(Display.getCurrent(), 59, 59, 59));
		shell.setForeground(new Color(Display.getCurrent(), 200, 200, 200));
		shell.setText("Inputs");
		shell.setSize(800, 300);
		centerShellOnScreen(shell);
	}
}