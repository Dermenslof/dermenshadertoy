package fr.dermenslof.shadertoy.ui.element;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Scale;
import org.eclipse.wb.swt.SWTResourceManager;

import fr.dermenslof.shadertoy.resource.data.IconInput;
import fr.dermenslof.shadertoy.sound.util.SoundCloud;
import fr.dermenslof.shadertoy.sound.util.SoundPlayer;

public class MiniPlayer extends Composite
{
	private SoundCloud	sd;
	public Button		play, rewind, change;
	public Scale		volume;
	public SoundPlayer	sPlayer;
	private Image		iplay, ipause, irewind;
	private Font		font;
	private boolean		mouseDown;
	private int			mouseX;
	private Thread		thread;

	public MiniPlayer(final Composite parent, int style)
	{
		super(parent, style);

		font = new Font(parent.getDisplay(), parent.getFont().getFontData()[0].getName(), 8, SWT.BOLD);
		iplay = SWTResourceManager.getImage(IconInput.PLAY.thumbnail);
		ipause = SWTResourceManager.getImage(IconInput.PAUSE.thumbnail);
		irewind = SWTResourceManager.getImage(IconInput.REWIND.thumbnail);

		play = new Button(parent, SWT.NONE);
		play.setImage(ipause);
		play.addSelectionListener(new SelectionListener()
		{
			@Override
			public void widgetSelected(SelectionEvent event)
			{
				if (sPlayer.player.isPlaying())
					sPlayer.player.pause();
				else
				{
					if (sPlayer.player.position() >= sPlayer.player.length() - 500)
						sPlayer.player.cue(0);
					sPlayer.player.play();
				}
				updateButton();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent event)
			{
			}
		});
		rewind = new Button(parent, SWT.NONE);
		rewind.setImage(irewind);
		rewind.addSelectionListener(new SelectionListener()
		{
			@Override
			public void widgetSelected(SelectionEvent event)
			{
				sPlayer.player.rewind();
				MiniPlayer.this.redraw();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent event) {}
		});

		change = new Button(parent, SWT.NONE);
		volume = new Scale(parent, SWT.NONE);
		volume.setMaximum(0);
		volume.setMaximum(100000);
		volume.setSelection(50000);
		volume.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent arg0)
			{
				sPlayer.setVolume(volume.getSelection());
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0)
			{
			}
		});
		PaintListener pl = new PaintListener()
		{
			@Override
			public void paintControl(PaintEvent event)
			{
				if (sPlayer == null || sPlayer.player == null)
					return;
				GC g = event.gc;
				Rectangle rec = MiniPlayer.this.getClientArea();
				int pos = sPlayer.player.position() * rec.width / sPlayer.player.length();
				if (mouseDown)
					pos = mouseX;
				int h = rec.y + sd.getBg().getBounds().height;
				g.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_DARK_RED));
				g.fillRectangle(rec.x, rec.y, rec.width, h);
				g.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_DARK_GRAY));
				g.fillRectangle(rec.x, rec.y, pos, h);
				g.drawImage(sd.getBg(), rec.x, rec.y);
				g.setBackground(new Color(Display.getCurrent(), 44, 44, 44));
				g.setFont(font);
				int x = rec.x + 80;
				int w = rec.width - 80;
				g.setBackground(new Color(Display.getCurrent(), 0, 0, 0));
				g.setForeground(new Color(Display.getCurrent(), 0, 88, 0));
				g.fillRectangle(x, rec.y + h, w, 22);
				String text = sd.getTitle();
				g.drawString(text, x + w - (int) ((System.currentTimeMillis() / 50) % (g.textExtent(text).x + w + 5)), h + 3);
				g.setBackground(new Color(Display.getCurrent(), 44, 44, 44));
				g.fillRectangle(rec.x, rec.y + h, 80, h + 5);
				g.setForeground(new Color(Display.getCurrent(), 255, 255, 255));
				String time = timeStampToString(sPlayer.player.position());
				if (mouseDown)
					time = timeStampToString(pos * sPlayer.player.length() / rec.width);
				g.drawString(time, rec.x + 5, h + 3);
			}

			private long[] values = { 3600000l, 60000l, 1000l, 1l };

			public String timeStampToString(long ts)
			{
				StringBuilder sb = new StringBuilder();
				long tmp, prev;
				int len = values.length;

				prev = ts;
				for (int i = 0; i < len; ++i)
				{
					tmp = prev / values[i];
					prev -= tmp * values[i];
					if (i == 3)
					{
						tmp = ts % 1000l;
						sb.append('.');
					}
					if (tmp == 0 && i != 2)
						continue;
					if (i > 0 && tmp < 10)
						sb.append('0');
					if (i == 3 && tmp < 100)
						sb.append('0');
					if (tmp >= 0 || (i == 3))
					{
						sb.append(tmp);
						switch (i)
						{
							case 0:
							case 1:
								sb.append(':');
								break;
							default:
								;
						}
					}
				}
				return sb.toString();
			}
		};
		addPaintListener(pl);
		addMouseListener(new MouseListener()
		{
			@Override
			public void mouseUp(MouseEvent event)
			{
				if (mouseDown)
				{
					mouseX = event.x;
					Rectangle rec = MiniPlayer.this.getClientArea();
					if (event.y < rec.y || event.y > rec.y + sd.getBg().getBounds().height)
						return;
					sPlayer.player.cue(event.x * sPlayer.player.length() / rec.width);
				}
				mouseDown = false;
			}

			@Override
			public void mouseDown(final MouseEvent event)
			{
				if (event.button != 1)
					return;
				Rectangle rec = MiniPlayer.this.getClientArea();
				if (event.y < rec.y || event.y > rec.y + sd.getBg().getBounds().height)
					return;
				mouseDown = true;
			}

			@Override
			public void mouseDoubleClick(MouseEvent event) {}
		}); 
		addMouseMoveListener(new MouseMoveListener()
		{
			@Override
			public void mouseMove(final MouseEvent event)
			{
				mouseX = event.x;
			}
		});
	}

	public void setIsVisible(boolean visible)
	{
		play.setVisible(visible);
		rewind.setVisible(visible);
		change.setVisible(visible);
		volume.setVisible(visible);
		setVisible(visible);
	}

	public void setSoundCloud(SoundCloud sd)
	{
		this.sd = sd;
		if (thread != null && thread.isAlive())
			thread.interrupt();
		if (sPlayer != null)
			sPlayer.close();
		sPlayer = null;
		if (sd != null && sd.getSound() != null && sd.getSound().exists())
		{
			sPlayer = new SoundPlayer();
			sPlayer.setup(sd.getSound().getPath());
			sPlayer.setVolume(volume.getSelection());
			sPlayer.player.loop();
			thread = new Thread("MiniPlayer: " + sd.getTitle())
			{
				@Override
				public void run()
				{
					while (sPlayer != null)
					{
						try{ Thread.sleep(100); }catch (Exception e) {}
						if (MiniPlayer.this.isDisposed() || MiniPlayer.this.getDisplay().isDisposed())
							break;
						if (!isReady())
							break;
						MiniPlayer.this.getDisplay().syncExec(new Runnable()
						{
							@Override
							public void run()
							{
								if (MiniPlayer.this.isDisposed() || !MiniPlayer.this.isVisible())
									return;
								MiniPlayer.this.redraw();
							}
						});
					}
					setSoundCloud(null);
				}
			};
			thread.start();
		}
	}

	public boolean isReady()
	{
		return sPlayer != null;
	}
	
	public void updateButton()
	{
		if (sPlayer != null && sPlayer.player.isPlaying())
			play.setImage(ipause);
		else
			play.setImage(iplay);
	}

	@Override
	protected void checkSubclass()
	{
	}

	@Override
	public void dispose()
	{
		if (isReady())
			setSoundCloud(null);
		super.dispose();
	}
}