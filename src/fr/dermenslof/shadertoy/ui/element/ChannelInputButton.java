package fr.dermenslof.shadertoy.ui.element;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

public class ChannelInputButton extends Button
{
	public Label					label;
	private TextureSelectionAdapter	selectionAdapter;
	
	public ChannelInputButton(Composite parent, int style, int channel)
	{
		super(parent, style);
		selectionAdapter = new TextureSelectionAdapter(channel, ShaderEditor.getApp());
		addSelectionListener(selectionAdapter);
		label = new Label(parent, SWT.NONE);
	}
	
	public void initLabel()
	{
		label.setAlignment(SWT.CENTER);
		label.setVisible(false);
		label.addMouseListener(new MouseListener()
		{
			@Override
			public void mouseUp(MouseEvent event)
			{
				selectionAdapter.widgetSelected();
			}
			
			@Override
			public void mouseDown(MouseEvent event) {}

			@Override
			public void mouseDoubleClick(MouseEvent event) {}
		});
	}

	@Override
	public void setImage(Image img)
	{
		label.setImage(img);
		if (img != null)
			setVisible(false, true);
		else
			label.setVisible(false);
	}
	
	@Override
	public void setVisible(boolean visible)
	{
		setVisible(visible, visible);
	}
	
	public void setVisible(boolean button, boolean label)
	{
		super.setVisible(button);
		this.label.setVisible(label);
	}
	
	@Override
	protected void checkSubclass() {}
}