package fr.dermenslof.shadertoy.ui.element;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.wb.swt.SWTResourceManager;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import fr.dermenslof.shadertoy.resource.ResourceManager;
import fr.dermenslof.shadertoy.resource.data.ChannelInput;
import fr.dermenslof.shadertoy.resource.data.IconInput;
import fr.dermenslof.shadertoy.sound.util.SoundCloud;

public class TextureSelectionAdapter extends SelectionAdapter
{
	public final int				channel;
	private final ShaderEditor		editor;	

	public TextureSelectionAdapter(int channel, ShaderEditor editor)
	{
		this.channel = channel;
		this.editor = editor;
	}

	@Override
	public void widgetSelected(SelectionEvent e)
	{
		widgetSelected();
	}
	
	public void widgetSelected()
	{
		Object obj = new SelectTexture(editor.shell, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL).open();
		if (obj instanceof String && ((String)obj).equals("none"))
		{
			editor.renderer.setSound(channel, null);
			if (editor.miniPlayers[channel] != null)
			{
				editor.miniPlayers[channel].setSoundCloud(null);
				editor.miniPlayers[channel].setIsVisible(false);
			}
			if (editor.channels[channel] == ChannelInput.WCAM)
				editor.renderer.disableWebCam(channel);
			editor.channels[channel] = null;
			editor.filters[channel].select(0);
			editor.wraps[channel].select(0);
			editor.boxes[channel].setSelection(true);
			editor.renderer.loadPNGTexture(null, channel);
			editor.buttons[channel].setImage(null);
			editor.buttons[channel].setVisible(true, false);
			return;
		}
		else if (obj instanceof SoundCloud)
		{
			if (editor.miniPlayers[channel] != null)
			{
				editor.miniPlayers[channel].setSoundCloud(null);
				editor.miniPlayers[channel].setIsVisible(false);
			}
			if (editor.channels[channel] == ChannelInput.WCAM)
				editor.renderer.disableWebCam(channel);
			editor.renderer.setSound(channel, null);
			editor.buttons[channel].setImage(null);
			editor.buttons[channel].setVisible(false, false);
			editor.channels[channel] = ChannelInput.SOUND;
			editor.filters[channel].select(0);
			editor.wraps[channel].select(0);
			editor.boxes[channel].setSelection(true);
			editor.miniPlayers[channel].setIsVisible(true);
			editor.miniPlayers[channel].setSoundCloud((SoundCloud)obj);
			editor.renderer.setSound(channel, editor.miniPlayers[channel].sPlayer);
			return;
		}
		ChannelInput result = (ChannelInput)obj;
		if (result != null)
		{
			editor.renderer.setSound(channel, null);
			if (result.type != ChannelInput.Type.WEBCAM)
			{
				Image image = result.big_thumbnail;
				editor.buttons[channel].setImage(image);
			}
			else
				editor.buttons[channel].setImage(SWTResourceManager.getImageResize(IconInput.WEBCAM.thumbnail, 64, 64));
			if (editor.channels[channel] == ChannelInput.WCAM)
				editor.renderer.disableWebCam(channel);
			if (editor.miniPlayers[channel] != null)
			{
				editor.miniPlayers[channel].setSoundCloud(null);
				editor.miniPlayers[channel].setIsVisible(false);
			}
			editor.channels[channel] = result;
			editor.filters[channel].select(0);
			editor.wraps[channel].select(0);
			editor.boxes[channel].setSelection(true);
			if (result.type == ChannelInput.Type.TEXTURE)
				editor.renderer.loadPNGTexture(ResourceManager.getAsInputStream(result.resource), channel);
			else if (result.type == ChannelInput.Type.CUBEMAP)
			{
				editor.wraps[channel].select(1);
				editor.boxes[channel].setSelection(false);
				editor.renderer.loadCubeMap(result.resource.getPath(), channel, GL11.GL_LINEAR, GL12.GL_CLAMP_TO_EDGE, false);
			}
			else if (result.type == ChannelInput.Type.WEBCAM)
				editor.renderer.enableWebCam(channel);
		}
	}
}