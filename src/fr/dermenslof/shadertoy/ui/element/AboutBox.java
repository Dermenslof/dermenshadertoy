package fr.dermenslof.shadertoy.ui.element;

import java.awt.Desktop;
import java.net.URL;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;

public class AboutBox extends Dialog
{
	public void centerShellOnScreen(final Shell shell)
	{
		final Rectangle shellBounds = shell.getBounds();
		final Display display = shell.getDisplay();
		final Monitor primaryMonitor = display.getPrimaryMonitor();
		final Rectangle monitorRect = primaryMonitor.getBounds();
		final int x = monitorRect.x + (monitorRect.width  - shellBounds.width)  / 2;
		final int y = monitorRect.y + (monitorRect.height - shellBounds.height) / 2;
		shell.setLocation(x, y);
	}

	public AboutBox()
	{
		super(Display.getDefault().getActiveShell());
	}
	
	@Override
	protected void createButtonsForButtonBar(final Composite parent)
	{ 
	  GridLayout layout = (GridLayout)parent.getLayout();
	  layout.marginHeight = 0;
	}

	@Override
	protected Control createDialogArea(final Composite parent)
	{
		final Composite dialogArea = (Composite)super.createDialogArea(parent);
		dialogArea.setBackground(new Color(Display.getCurrent(), 59, 59, 59));
		dialogArea.setForeground(new Color(Display.getCurrent(), 200, 200, 200));
		Display display = Display.getCurrent();
		Link link = new Link(dialogArea, SWT.BORDER);
		String url = "https://bitbucket.org/Dermenslof/dermenshadertoy/issues?status=new&status=open";
	    String message = "Version: beta 0.3\n"
	    		+ "For any bug, please report <a href=\"" + url + "\">  Here  </a>\n"
	    		+ "\n"
	    		+ "Copyright © 2015 DermenShadertoy -- All Rights Reserved";
	    link.setBackground(new Color(Display.getCurrent(), 59, 59, 59));
		link.setForeground(new Color(Display.getCurrent(), 200, 200, 200));
	    link.setText(message);
	    link.setSize(400, 100);
	    link.setToolTipText(url);
	    link.setCursor(display.getSystemCursor(SWT.CURSOR_HAND));
	    link.addSelectionListener(new SelectionAdapter()
	    {
	    	@Override
	    	public void widgetSelected(SelectionEvent event)
	    	{
	    		Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
	    	    if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE))
	    	    {
	    	        try
	    	        {
	    	            desktop.browse(new URL(event.text).toURI());
	    	        }
	    	        catch (Exception e)
	    	        {
	    	            e.printStackTrace();
	    	        }
	    	    }
	    	}
	    });
	    link.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, true, true));
		return dialogArea;
	}

	@Override
	protected void configureShell(final Shell shell)
	{
		super.configureShell(shell);
		shell.setBackground(new Color(Display.getCurrent(), 59, 59, 59));
		shell.setForeground(new Color(Display.getCurrent(), 200, 200, 200));
		shell.setText("About");
		shell.setSize(450, 200);
		centerShellOnScreen(shell);
	}
}