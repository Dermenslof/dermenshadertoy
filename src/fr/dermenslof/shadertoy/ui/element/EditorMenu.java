package fr.dermenslof.shadertoy.ui.element;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URI;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.widgets.ExpandBar;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;

public class EditorMenu
{
	private Menu menu;

	public EditorMenu(final Shell shell)
	{
		menu = new Menu(shell, SWT.BAR);
		MenuItem item = new MenuItem(menu, SWT.CASCADE);
		item.setText("File");
		Menu dropMenu = new Menu(shell, SWT.DROP_DOWN);
		item.setMenu(dropMenu);
		item = new MenuItem(dropMenu, SWT.NULL);
		item.setText("Open...\tCtrl+O");
		item.setAccelerator(SWT.CTRL + 'O');
		item.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent event)
			{
				ShaderEditor.getApp().openFile();
			}
		});
		new MenuItem(dropMenu, SWT.SEPARATOR);
		item = new MenuItem(dropMenu, SWT.NULL);
		item.setText("Exit\tAlt+F4");
		item.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent event)
			{
				ShaderEditor.getApp().shell.close();
			}
		});
		item = new MenuItem(menu, SWT.CASCADE);
		item.setText("Shader");
		dropMenu = new Menu(shell, SWT.DROP_DOWN);
		item.setMenu(dropMenu);
		item = new MenuItem(dropMenu, SWT.NULL);
		item.setText("Toggle FullScreen\tF11");
		item.setAccelerator(SWT.F11);
		item.addSelectionListener(new SelectionAdapter()
		{
			@Override
			public void widgetSelected(SelectionEvent e)
			{
				Shell sh = ShaderEditor.getApp().shell;
				sh.setFullScreen(!sh.getFullScreen());
			}
		});
		item = new MenuItem(dropMenu, SWT.NULL);
		item.setText("Toggle Tools\tF12");
		item.setAccelerator(SWT.F12);
		item.addSelectionListener(new SelectionAdapter()
		{
			@Override
			public void widgetSelected(SelectionEvent e)
			{
				ShaderEditor se = ShaderEditor.getApp();
				se.tools.setVisible(!se.tools.isVisible());
				for (int i = 0; i < 4; ++i)
				{
					if (!se.buttons[i].isVisible())
					{
						if (se.miniPlayers[i].isReady())
							se.miniPlayers[i].setIsVisible(true);
						else
							se.buttons[i].setVisible(true);
						se.items[i].setExpanded(false);
						ExpandBar b = se.options[i];
						Rectangle rec = se.buttons[i].getBounds();
						b.setBounds(rec.x, rec.y + rec.height, 182, 30);
					}
				}
				FormData data = (FormData) se.renderer.getLayoutData();
				if (!se.tools.isVisible())
				{
					shell.setMinimumSize(2, 2);
					data.bottom = new FormAttachment(100);
				}
				else
				{
					shell.setMinimumSize(802, 270);
					data.bottom = new FormAttachment(se.tools);
				}
				boolean fullscreen = se.shell.getFullScreen();
				Rectangle rec = se.shell.getBounds();
				se.shell.setSize(rec.width, se.shell.getSize().y + 1);
				se.shell.setSize(rec.width, se.shell.getSize().y - 1);
				se.shell.setFullScreen(fullscreen);
			}
		});
		item = new MenuItem(dropMenu, SWT.NULL);
		item.setText("Run\tAlt+Enter");
		item.setAccelerator(SWT.ALT + SWT.CR);
		item.addSelectionListener(new SelectionAdapter()
		{
			@Override
			public void widgetSelected(SelectionEvent e)
			{
				ShaderEditor.getApp().reloadShader();
			}
		});
		
		item = new MenuItem(menu, SWT.CASCADE);
		item.setText("Help");
		dropMenu = new Menu(shell, SWT.DROP_DOWN);
		item.setMenu(dropMenu);
		item = new MenuItem(dropMenu, SWT.NULL);
		item.setText("Inputs\tCtrl+Shift+H");
		item.setAccelerator(SWT.CTRL + SWT.SHIFT + 'H');
		item.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent event)
			{
				new InputsBox().open();
			}
		});
		
		item = new MenuItem(dropMenu, SWT.NULL);
		item.setText("Documentation\tCtrl+Shift+D");
		item.setAccelerator(SWT.CTRL + SWT.SHIFT + 'D');
		item.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent event)
			{
				if(Desktop.isDesktopSupported())
				{
					if(Desktop.getDesktop().isSupported(Desktop.Action.OPEN))
					{
						try
						{
							Desktop.getDesktop().open(new File(System.getProperty("user.dir"), "resources/GLSL_ES_Specification_3.20.pdf"));
						}
						catch (IOException ex)
						{
							
						}
					}
					else if (Desktop.getDesktop().isSupported(Desktop.Action.BROWSE))
					{
						try
						{
							Desktop.getDesktop().browse(new URI("https://www.khronos.org/registry/gles/specs/3.2/GLSL_ES_Specification_3.20.withchanges.pdf"));
						}
						catch (Exception e)
						{
							
						}
					}
				}
				else
				{
					// todo open manualy the pdf in a new shell
				}
			}
		});
		
		item = new MenuItem(dropMenu, SWT.NULL);
		item.setText("About\tCtrl+H");
		item.setAccelerator(SWT.CTRL + 'H');
		item.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent event)
			{
				new AboutBox().open();
			}
		});
	}

	public Menu getMenu()
	{
		return menu;
	}
}