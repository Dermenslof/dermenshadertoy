package fr.dermenslof.shadertoy.ui.element;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;

public class SplashWindow
{
	protected int percent = 0;

	public SplashWindow(Display display)
	{
		final Image image = new Image(display, System.getProperty("user.dir") + "/resources/icon.png");
		final Shell splash = new Shell(SWT.ON_TOP);
		final ProgressBar bar = new ProgressBar(splash, SWT.NONE);
		bar.setMaximum(100);

		Label label = new Label(splash, SWT.NONE);
		label.setImage(image);

		FormLayout layout = new FormLayout();
		splash.setLayout(layout);

		FormData labelData = new FormData();
		labelData.right = new FormAttachment(100, 0);
		labelData.bottom = new FormAttachment(100, 0);
		label.setLayoutData(labelData);

		FormData progressData = new FormData();
		progressData.left = new FormAttachment(0, -5);
		progressData.right = new FormAttachment(100, 0);
		progressData.bottom = new FormAttachment(100, 0);
		bar.setLayoutData(progressData);
		splash.pack();

		Rectangle splashRect = splash.getBounds();
		Rectangle displayRect = display.getBounds();
		int x = (displayRect.width - splashRect.width) / 2;
		int y = (displayRect.height - splashRect.height) / 2;
		splash.setLocation(x, y);
		splash.open();

		display.asyncExec(new Runnable()
		{
			public void run()
			{
				while (percent != 100)
				{
					try {

						Thread.sleep(100);
					}
					catch(InterruptedException e) {

						e.printStackTrace();
					}
					bar.setSelection(percent);
				}
				splash.close();
				image.dispose();
			}
		});
	}
}