package fr.dermenslof.shadertoy.sound.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.eclipse.swt.graphics.Image;
import org.eclipse.wb.swt.SWTResourceManager;
import org.json.JSONObject;

import com.soundcloud.api.ApiWrapper;
import com.soundcloud.api.Http;
import com.soundcloud.api.Request;
import com.soundcloud.api.Stream;

public class SoundCloud extends Thread
{
	public static final File	WRAPPER_SER	= new File("wrapper.ser");
	private static ApiWrapper			wrapper;
	private String				url;
	private int					duration;
	private String				title;
	private int					bitRate;
	private File				sound;
	private Image				bg;
	public int					progress	= 0;
	public static String				error;

	public SoundCloud(String url)
	{
		super("SoundCloud");
		this.url = url;
	}

	public static boolean init()
	{
		if (!WRAPPER_SER.exists())
		{
			error = "\nThe serialised wrapper (" + WRAPPER_SER + ") does not exist.\n" + "Run CreateWrapper first to create it.";
			return false;
		}
		boolean login = false;
		try
		{
			wrapper = ApiWrapper.fromFile(WRAPPER_SER);
			login = wrapper.login("DermenShadertoy", "shadertoy") != null;
			wrapper.toFile(WRAPPER_SER);
		}
		catch (ClassNotFoundException | IOException e)
		{
			error = e.getMessage();
			e.printStackTrace();
			return false;
		}
		return login;
	}

	@Override
	public void run()
	{
		prepare();
	}

	private void prepare()
	{
		error = null;
		String request = "https://soundcloud.com/oembed?format=json&url=" + url;
		Request resource = Request.to(request);
		try
		{
			wrapper.login("DermenShadertoy", "shadertoy");
			HttpResponse resp = wrapper.get(resource);
			if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK)
			{
				long track = wrapper.resolve(url);
				resource = Request.to("https://api.soundcloud.com/tracks/" + track);
				resp = wrapper.get(resource);
				if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK)
				{
					JSONObject o = new JSONObject(Http.formatJSON(Http.getString(resp)));
					if (!o.getBoolean("streamable"))
						throw new Exception("this sound is not streamable");
					downloadBg(o.getString("waveform_url"));
					duration = o.getInt("duration");
					title = o.getString("title");
					Stream stm = wrapper.resolveStreamUrl(o.getString("stream_url"), true);
					bitRate = stm.bitRate;
					downloadMp3(stm.streamUrl, stm.contentLength);
				}
				else
					error = "sound not found: Bad url";
			}
			else
				error = "sound not found: Bad url";
		}
		catch (Exception e)
		{
			error = e.getMessage();
			e.printStackTrace();
		}
		finally
		{
			try
			{
				wrapper.toFile(WRAPPER_SER);
			}
			catch (IOException e)
			{
				;
			}
		}
	}

	public int getDuration()
	{
		return duration;
	}

	public int getBitRate()
	{
		return bitRate;
	}

	public File getSound()
	{
		return sound;
	}

	public Image getBg()
	{
		return bg;
	}

	public String getTitle()
	{
		return title;
	}

	@SuppressWarnings("resource")
	private void downloadMp3(String uri, long length) throws IOException
	{
		String name = title.trim().replaceAll("/", "\\\\");
		BufferedInputStream fis = new BufferedInputStream(new URL(uri).openStream());
		FileOutputStream fos = new FileOutputStream(name + ".mp3");
		byte data[] = new byte[1024];
		int count;
		long lCount = 0;
		progress = 0;
		while ((count = fis.read(data, 0, 1024)) != -1)
		{
			lCount += count;
			progress = (int) (100L * lCount / length);
			fos.write(data, 0, count);
		}
		progress = 100;
		sound = new File(name + ".mp3");
		sound.deleteOnExit();
	}

	@SuppressWarnings("resource")
	private void downloadBg(String uri) throws IOException
	{
		String[] tab = uri.split("/");
		String name = tab[tab.length - 1];
		ReadableByteChannel rbc = Channels.newChannel(new URL(uri).openStream());
		FileOutputStream fos = new FileOutputStream(name);
		fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
		new File(name).deleteOnExit();
		bg = SWTResourceManager.getImageResize(new FileInputStream(new File(name)), 180, 112);
	}
}