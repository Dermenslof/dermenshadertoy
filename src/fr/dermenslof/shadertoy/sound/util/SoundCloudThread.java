package fr.dermenslof.shadertoy.sound.util;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.ProgressBar;

import fr.dermenslof.shadertoy.ui.element.SelectTexture;
import fr.dermenslof.shadertoy.ui.element.ShaderEditor;

public class SoundCloudThread extends Thread
{
	private SoundCloud		sd;
	private ProgressBar		bar;
	private Display			display;
	private SelectTexture	st;

	public SoundCloudThread(Display display, SoundCloud sd, ProgressBar bar, SelectTexture st)
	{
		super("soundcloud");
		this.display = display;
		this.sd = sd;
		this.bar = bar;
		this.st = st;
	}

	@Override
	public void run()
	{
		if (display.isDisposed())
			return;
		display.syncExec(new Runnable() {
			@Override
			public void run()
			{
				if (bar.isDisposed())
					return;
				bar.setVisible(true);
				bar.setSelection(0);
			}
		});
		if (SoundCloud.init())
		{
			sd.start();
			while (sd.isAlive())
			{
				if (display.isDisposed())
					return;
				display.syncExec(new Runnable() {
					@Override
					public void run()
					{
						if (bar.isDisposed())
							return;
						bar.setSelection(sd.progress);
					}
				});
			}
		}
		if (SoundCloud.error != null)
		{
			if (display.isDisposed())
				return;
			display.asyncExec(new Runnable() {
				@Override
				public void run()
				{
					bar.setVisible(false);
					ShaderEditor.getApp().showError(bar.getShell(), SoundCloud.error);
					SoundCloud.error = null;
				}
			});
		}
		else
		{
			display.asyncExec(new Runnable() {
				@Override
				public void run()
				{
					st.result = sd;
					st.shell.close();
				}
			});
		}
	}
}