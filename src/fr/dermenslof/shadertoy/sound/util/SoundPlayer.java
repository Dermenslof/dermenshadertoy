package fr.dermenslof.shadertoy.sound.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import javax.sound.sampled.FloatControl;

import ddf.minim.AudioPlayer;
import ddf.minim.Controller;
import ddf.minim.Minim;
import ddf.minim.analysis.DFT;

public class SoundPlayer
{
	public Minim		minim;
	public AudioPlayer	player;
	public boolean		hasVolume;
	public DFT			fft;

	@SuppressWarnings("deprecation")
	public boolean setup(String path)
	{
		minim = new Minim(this);
		if (minim == null)
			return false;
		player = minim.loadFile(path, 1024);
		if (player != null)
		{
			hasVolume = player.hasControl(Controller.VOLUME);
			fft = new DFT(player.bufferSize(), player.sampleRate());
		}
		return player != null;
	}

	public void close()
	{
		player.close();
		minim.stop();
	}

	@SuppressWarnings("deprecation")
	public void setVolume(float v)
	{
		if (player == null)
			return;
		if (hasVolume)
			player.setVolume(v);
		else
		{
			FloatControl control;
			try
			{
				control = (FloatControl) player.getControl(FloatControl.Type.MASTER_GAIN);
				control.setValue(v == 0f ? -80f : 26.0206f * v / 100000f - 20f);
			}
			catch (Exception ex)
			{
				try
				{
					control = (FloatControl) player.getControl(FloatControl.Type.VOLUME);
					control.setValue(v);
				}
				catch (Exception ex2)
				{

				}
			}
		}
	}

	public String sketchPath(String fileName)
	{
		return "./";
	}

	public InputStream createInput(String path) throws FileNotFoundException
	{
		return new FileInputStream(new File(path));
	}
}
