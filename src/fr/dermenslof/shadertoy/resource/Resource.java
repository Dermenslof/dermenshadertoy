package fr.dermenslof.shadertoy.resource;

public abstract interface Resource
{
	public abstract String getPath();
}