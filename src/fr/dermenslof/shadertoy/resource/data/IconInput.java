package fr.dermenslof.shadertoy.resource.data;

import fr.dermenslof.shadertoy.resource.BasicResource;
import fr.dermenslof.shadertoy.resource.Resource;

public enum IconInput
{
	PLAY(Type.ICON, new TextureResource("play.png")),
	PAUSE(Type.ICON, new TextureResource("pause.png")),
	REWIND(Type.ICON, new TextureResource("rewind.png")),
	WEBCAM(Type.ICON, new TextureResource("webcam.png"));

	public static final IconInput ICON[] =
	{
			PLAY,
			PAUSE,
			REWIND,
			WEBCAM,
	};

	public enum Type
	{
		ICON,
	};

	private static class TextureResource extends BasicResource
	{
		TextureResource(String name)
		{
			super("resources/icons/" + name);
		}
	}

	public final Type		type;
	public final Resource	resource;
	public final Resource	thumbnail;

	IconInput(Type type, Resource resource)
	{
		this(type, resource, resource);
	}

	IconInput(Type type, Resource resource, Resource thumbnail)
	{
		this.type = type;
		this.resource = resource;
		this.thumbnail = thumbnail;
	}
}
