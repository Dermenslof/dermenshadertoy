package fr.dermenslof.shadertoy.resource.data;

import java.io.IOException;

import org.eclipse.swt.graphics.Image;
import org.eclipse.wb.swt.SWTResourceManager;

import fr.dermenslof.shadertoy.resource.BasicResource;
import fr.dermenslof.shadertoy.resource.Resource;

public enum ChannelInput
{
	TEX00(Type.TEXTURE, new TextureResource("tex00.png")),
	TEX01(Type.TEXTURE, new TextureResource("tex01.png")),
	TEX02(Type.TEXTURE, new TextureResource("tex02.png")),
	TEX03(Type.TEXTURE, new TextureResource("tex03.png")),
	TEX04(Type.TEXTURE, new TextureResource("tex04.png")),
	TEX05(Type.TEXTURE, new TextureResource("tex05.png")),
	TEX06(Type.TEXTURE, new TextureResource("tex06.png")),
	TEX07(Type.TEXTURE, new TextureResource("tex07.png")),
	TEX08(Type.TEXTURE, new TextureResource("tex08.png")),
	TEX09(Type.TEXTURE, new TextureResource("tex09.png")),
	TEX10(Type.TEXTURE, new TextureResource("tex10.png")),
	TEX11(Type.TEXTURE, new TextureResource("tex11.png")),
	TEX12(Type.TEXTURE, new TextureResource("tex12.png")),
	TEX14(Type.TEXTURE, new TextureResource("tex14.png")),
	TEX15(Type.TEXTURE, new TextureResource("tex15.png")),
	TEX16(Type.TEXTURE, new TextureResource("tex16.png")),
	TEX17(Type.TEXTURE, new TextureResource("tex17.png")),
	TEX18(Type.TEXTURE, new TextureResource("tex18.png")),
	TEX19(Type.TEXTURE, new TextureResource("tex19.png")),
	TEX20(Type.TEXTURE, new TextureResource("tex20.png")),

	CUBE00(Type.CUBEMAP, new CubemapResource("cube00_0.png")),
	CUBE01(Type.CUBEMAP, new CubemapResource("cube01_0.png")),
	CUBE02(Type.CUBEMAP, new CubemapResource("cube02_0.png")),
	CUBE03(Type.CUBEMAP, new CubemapResource("cube03_0.png")),
	CUBE04(Type.CUBEMAP, new CubemapResource("cube04_0.png")),
	CUBE05(Type.CUBEMAP, new CubemapResource("cube05_0.png")),
	
	WCAM(Type.WEBCAM),
	
	SOUND(Type.SOUND);

	public static final ChannelInput TEXTURES[] =
	{
		TEX00, TEX01, TEX02, TEX03,
		TEX04, TEX05, TEX06, TEX07,
		TEX08, TEX09, TEX10, TEX11,
		TEX12, TEX14, TEX15, TEX16,
		TEX17, TEX18, TEX19,TEX20,
	};

	public static final ChannelInput CUBEMAPS[] =
	{
		CUBE00, CUBE01, CUBE02,
		CUBE03, CUBE04, CUBE05,
	};
	
	public static final ChannelInput WEBCAM[] =
	{
		WCAM,
	};
	
	public static final ChannelInput SOUNDS[] =
	{
		SOUND,
	};

	public enum Type
	{
		TEXTURE, CUBEMAP, WEBCAM, SOUND,
	};

	private static class TextureResource extends BasicResource
	{
		TextureResource(String name)
		{
			super("resources/textures/" + name);
		}
	}
	
	private static class CubemapResource extends BasicResource
	{
		CubemapResource(String name)
		{
			super("resources/cubemaps/" + name);
		}
	}

	public final Type		type;
	public final Resource	resource;
	public Image			big_thumbnail;
	public Image			small_thumbnail;

	private ChannelInput(Type type)
	{
		this(type, null, null);
	}
	
	private ChannelInput(Type type, Resource resource)
	{
		this(type, resource, resource);
	}

	private ChannelInput(Type type, Resource resource, Resource thumbnail)
	{
		this.type = type;
		this.resource = resource;
		if (resource == null)
			return;
	}
	
	public static void init()
	{
		for (ChannelInput input : ChannelInput.CUBEMAPS)
			init(input);
		for (ChannelInput input : ChannelInput.TEXTURES)
			init(input);
	}
	
	private static void init(ChannelInput input)
	{
		if (input.resource == null)
			return;
		try
		{
			input.big_thumbnail = SWTResourceManager.getImageResize(SWTResourceManager.class.getResourceAsStream("/" + input.resource.getPath()), 180, 110);
			input.small_thumbnail = SWTResourceManager.getImageResize(SWTResourceManager.class.getResourceAsStream("/" + input.resource.getPath()), 64, 64);
		}
		catch (IOException e)
		{
			input.big_thumbnail = SWTResourceManager.getImage(input.resource.getPath());
			input.small_thumbnail = SWTResourceManager.getImage(input.resource.getPath());
		}
	}
}
