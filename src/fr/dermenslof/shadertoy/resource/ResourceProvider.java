package fr.dermenslof.shadertoy.resource;

import java.io.InputStream;
import java.nio.ByteBuffer;

public abstract interface ResourceProvider
{
	public abstract long getLastModified(Resource paramResource);

	public abstract String getAsString(Resource paramResource);

	public abstract ByteBuffer getAsByteBuffer(Resource paramResource);

	public abstract InputStream getAsInputStream(Resource paramResource);
}