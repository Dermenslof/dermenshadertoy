package fr.dermenslof.shadertoy.resource;

public class BasicResource implements Resource
{
	private final String path;

	public BasicResource(String path)
	{
		this.path = path;
	}

	public String getPath()
	{
		return this.path;
	}
}