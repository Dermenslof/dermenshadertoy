package fr.dermenslof.shadertoy.resource;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

import com.google.common.base.Charsets;
import com.google.common.io.ByteStreams;

public abstract class InputStreamResourceProvider implements ResourceProvider
{
	@Override
	public String getAsString(Resource r)
	{
		try
		{
			InputStream is = this.getAsInputStream(r);
			Throwable throwable = null;
			try
			{
				String string = new String(ByteStreams.toByteArray((InputStream) is), Charsets.UTF_8);
				return string;
			}
			catch (Throwable t)
			{
				throwable = t;
				throw t;
			}
			finally
			{
				if (is != null)
				{
					if (throwable != null)
					{
						try
						{
							is.close();
						}
						catch (Throwable t)
						{
							throwable.addSuppressed(t);
						}
					}
					else
					{
						is.close();
					}
				}
			}
		}
		catch (IOException e)
		{
			throw new IllegalStateException(e);
		}
	}

	@Override
	public ByteBuffer getAsByteBuffer(Resource r)
	{
		try
		{
			InputStream is = this.getAsInputStream(r);
			Throwable throwable = null;
			try
			{
				ByteBuffer byteBuffer = ByteBuffer.wrap(ByteStreams.toByteArray((InputStream) is));
				return byteBuffer;
			}
			catch (Throwable t)
			{
				throwable = t;
				throw t;
			}
			finally
			{
				if (is != null)
				{
					if (throwable != null)
					{
						try
						{
							is.close();
						}
						catch (Throwable t)
						{
							throwable.addSuppressed(t);
						}
					}
					else
					{
						is.close();
					}
				}
			}
		}
		catch (IOException e)
		{
			throw new IllegalStateException(e);
		}
	}
}